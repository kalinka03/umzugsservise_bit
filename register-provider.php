<?php include("template/header-main.php") ?>

<div class="section-register">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="register-provider">
                    <div class="block-prov">
                        <h2>Регистрация на сайте</h2>
                        <div class="steps">
                            <div class="step-menu nav-tabs">
                                <button disabled class="sk_tab filter active">
                                    <span>1</span>
                                </button>
                                <button disabled class="sk_tab filter">
                                    <span>2</span>
                                </button>
                                <button disabled class="sk_tab filter">
                                    <span>3</span>
                                </button>
                            </div>
                            <div class="line-provider"></div>
                        </div>
                    </div>
                    <form class="form-group form-all f-prof">
                        <div class="step-content">
                            <div class="content-choice sk_prov_content active">
                                <input type="hidden" name="Регистрация на сайте поставщик">
                                <p>Заполните поля формы</p>
                                <h4>Компания:</h4>
                                <div class="center-block-provider tab1_register">
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Название компании:</label>
                                                <input type="text" name="name_company"
                                                       placeholder="Название компании">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Выберите форму регистрации:</label>
                                                <select class="change_order" name="registration" required="required">
                                                    <option selected disabled>Выберите форму регистрации</option>
                                                    <option value="Natürliche Person / Einzelunternehmen">Natürliche
                                                        Person / Einzelunternehmen
                                                    </option>
                                                    <option value="GmbH">GmbH</option>
                                                    <option value="AG">AG</option>
                                                    <option value="GbR">GbR</option>
                                                    <option value="UG (haftungsbeschränkt)">UG (haftungsbeschränkt)
                                                    </option>
                                                    <option value="EK (Einzelkaufmann) / eU (Einzelunternehmer)">EK
                                                        (Einzelkaufmann) / eU (Einzelunternehmer)
                                                    </option>
                                                    <option value="eingetragener Kaufmann (e.K)">eingetragener Kaufmann
                                                        (e.K)
                                                    </option>
                                                    <option value="oHG, KG (einschl. GmbH &amp; Co.KG)">oHG, KG
                                                        (einschl. GmbH &amp; Co.KG)
                                                    </option>
                                                    <option value="Ltd.">Ltd.</option>
                                                    <option value="Sonstige Rechtsform">Sonstige Rechtsform</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Укажите количество сотрудников:</label>
                                                <select class="change_order" name="company_employees"
                                                        required="required">
                                                    <option selected disabled>Укажите количество сотрудников</option>
                                                    <option value="0-10">0-10</option>
                                                    <option value="11-50">11-50</option>
                                                    <option value="51-100">51-100</option>
                                                    <option value="100+">100+</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Сайт компании:</label>
                                                <input class=sites_company" type="text" name="sites_company"
                                                       placeholder="Сайт компании">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="all-profile foto-prov logo-company-foto">
                                        <div class="r-all">
                                            <label class="all-label">Логотип компании:</label>
                                            <div class="border-foto">
                                                <div class="new-fot add-plus">
                                                    <label for="uploadbtn" class="uploadButton"></label>
                                                    <input style="opacity: 0; z-index: -1;" type="file"
                                                           id="uploadbtn">
                                                </div>

                                                <div class="img-grup img-wrapp__none_img">
                                                    <div class="img-wrapp">
                                                        <input type="file" style="display: none;" name="foto_company"
                                                               accept="image/jpeg,image/png,image/jpg">
                                                        <span><span class="clouse-bg"></span></span>
                                                        <img src="">
                                                        <div class="del-img">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="register_document">Регистрационные документы:</p>
                                    <p class="valid_formats_pr">Допустимыфе форматы документов для загрузки: jpg,
                                        pdf.</p>
                                    <div class="frame">
                                        <div class="wrapper-table">
                                            <div class="wrapper-table-row">
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Gewerbeanmeldung</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name="document_dounload[]"
                                                                       id="doc_1">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">USt-IdNr.t</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name="document_dounload[]"
                                                                       id="doc_2">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Transportversicherung</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name="document_dounload[]"
                                                                       id="doc_3">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Betriebshaftpflichtversicherung</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name="document_dounload[]"
                                                                       id="doc_4">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Verkehrshaftpflichtversicherung</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name="document_dounload[]"
                                                                       id="doc_5">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Handelsregistereintrag</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Steuernummer</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Güterkraftverkehrserlaubnis /
                                                        EU-Lizenz
                                                    </div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Mitglied AMÖ</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Mitglied DMS</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Mitglied DMS</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="but-btn login-btn bot-btn btnNextprov">
                                        <button>Далее</button>
                                    </div>
                                </div>
                            </div>
                            <div class="content-choice sk_content">
                                <p>Заполните поля формы</p>
                                <h4>Класификация компании:</h4>
                                <div class="clasification-descript">
                                    <p>Для подтверждения допуска к выполнению услуг, предоставьте подтверждающие
                                        документы. Допуск на предоставление услуг влияет на отображение заявок.
                                        Допустимыфе форматы документов для загрузки: jpg, pdf.</p>
                                </div>
                                <div class="center-block-provider tab2_register">
                                    <div class="frame">
                                        <div class="wrapper-table">
                                            <div class="wrapper-table-row">
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Möbelmontageservice</div>
                                                    <div class="table-col-l table-condition"> при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Küchenmontageservice Schreiner / Tischler
                                                    </div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name='document_dounload_tabNext[]'
                                                                       id="doc_6">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col" data-aos="zoom-in-right"
                                                     data-aos-delay="100"
                                                     data-aos-once="true">
                                                    <div class="table-col-l">Nachweis</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name='document_dounload_tabNext[]'
                                                                       id="doc_7">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Elektroservice / Nachweis</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name='document_dounload_tabNext[]'
                                                                       id="doc_8">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Wasserinstallation / Nachweis</div>
                                                    <div class="table-col-l table-condition">при наличии</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload ">
                                                            <label>
                                                                <input type="file" name="file">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">Einlagerungsservice</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name='document_dounload_tabNext[]'
                                                                       id="doc_9">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                                <div class="wrapper-table-col">
                                                    <div class="table-col-l">EDF – Service</div>
                                                    <div class="table-col-l table-condition"> обязательно *</div>
                                                    <div class="table-col-s">
                                                        <div class="file-upload register_doc_provider">
                                                            <label>
                                                                <input type="file" name='document_dounload_tabNext[]'
                                                                       id="doc_10">
                                                                <span>Загрузить</span>
                                                            </label>
                                                        </div>
                                                        <input id="filename">
                                                    </div>
                                                    <div class="table-col-r">
                                                        <a href="#">Удалить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-butProv">
                                        <div class="but-btn login-btn bot-btn btnPrevProv">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn login-btn bot-btn btnNextprov">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-choice sk_content third_sk_content">
                                <p>Заполните поля формы</p>
                                <h4>Адрес компании и контактные данные:</h4>
                                <div class="center-block-provider  tab3_register">
                                    <input type="hidden" name="Регистрация на сайте клиент">
                                    <div class="recourse-lk">
                                        <div class="item">
                                            <p>Контактное лицо:</p>
                                        </div>
                                        <div class="recourse-name">
                                            <div class="item">
                                                <label>
                                                    <input class="radio" type="radio" name="radio-test" value="Господин"
                                                           checked>
                                                    <span class="radio-custom"></span>
                                                    <span class="label">Господин</span>
                                                </label>
                                            </div>
                                            <div class="item">
                                                <label>
                                                    <input class="radio" type="radio" name="radio-test" value="Госпожа">
                                                    <span class="radio-custom"></span>
                                                    <span class="label">Госпожа</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="center-input">
                                        <div class="block-profile">
                                            <div class="v-d">
                                                <input class="popup-call__name" type="text" name="name"
                                                       placeholder="Имя">
                                            </div>
                                            <div class="v-d">
                                                <input class="popup-call__name" type="text" name="surname"
                                                       placeholder="Фамилия">
                                            </div>
                                        </div>
                                        <label class="all-label">Адрес:</label>
                                        <div class="block-profile">
                                            <div class="adress-pr val_adress">
                                                <div class="v-d-index">
                                                    <input class="index" type="tel" name="index" placeholder="Индекс"
                                                           maxlength="5">
                                                </div>
                                                <div class="v-d">
                                                    <input class="city" type="text" name="city" placeholder="Город">
                                                </div>
                                            </div>
                                            <div class="v-d">
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Адрес">
                                            </div>
                                        </div>
                                        <label class="all-label">Контактные данные:</label>
                                        <div class="block-profile">
                                            <div class="v-d">
                                                <input class="popup-call__email" type="email" name="email"
                                                       placeholder="Email">
                                            </div>
                                            <div class="v-d">
                                                <input class="form-control" placeholder="Телефон"
                                                       name="phone" id="Piramida_phone-qust" type="tel" maxlength="20"/>
                                                <div class="help-block error" id="Piramida_phone_em_"
                                                     style="display:none"></div>
                                                <input type="checkbox" id="phone_mask-qust"
                                                       placeholder="Телефон" checked>
                                                <label id="descr" for="phone_mask2" class="phone-mask2">Использовать
                                                    маску</label>
                                            </div>
                                        </div>
                                        <label class="all-label">Пароль:</label>
                                        <div class="block-profile password_validate">
                                            <div class="v-d">
                                                <input id="password1" type="password" name="password_new"
                                                       placeholder="Новый пароль">
                                            </div>
                                            <div class="v-d">
                                                <input id="password2" class="mob_style_validate" type="password"
                                                       name="password_repeat"
                                                       placeholder="Повторить пароль">
                                            </div>
                                        </div>
                                        <p id="validate-status"></p>
                                        <label class="all-label foto-provider-mobile">Фото:</label>
                                        <div class="provider-foto-company foto-prov">
                                            <div class="foto-block border-foto">
                                                <div class="new-fot add-plus">
                                                    <label for="uploadbtn" class="uploadButton"></label>
                                                    <input style="opacity: 0; z-index: -1;" type="file"
                                                           name="upload" id="uploadbtn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-to-accept prov-accept">
                                            <div class="left-col">
                                                <p>Регистрируясь на сайте, я принимаю</p>
                                            </div>
                                            <div class="right-col">
                                                <a href="#js-terms-use">Пользовательское соглашение</a>
                                            </div>
                                        </div>
                                        <div class="register-butProv">
                                            <div class="but-btn login-btn bot-btn btnPrevProv">
                                                <button>Назад</button>
                                            </div>
                                            <div class="but-btn login-btn bot-btn btnSubmit">
                                                <button>Регистрация</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/register_provider/mainController.js"></script>
<script type="text/javascript" src="/js/register_provider/tabContent.js"></script>
<script type="text/javascript" src="/js/register_provider/downloadPhoto.js"></script>
<script type="text/javascript" src="/js/register_client/passwordMatching.js"></script>
<script type="text/javascript" src="/js/register_provider/validateController.js"></script>
<script type="text/javascript" src="/js/lk_other/validateNumber.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/uploadPhoto.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

<?php include("template/header-main.php") ?>

<div class="section-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="entrance-login">
                    <h2>Вход на сайт</h2>
                    <div class="center-info">
                        <p>Заполните поля</p>
                        <form class="form-all form-group login_valid">
                            <input type="hidden" name="Вход на сайт">
                            <div>
                                <input class="popup-call__email" type="text" name="email" placeholder="Email">
                            </div>
                            <div>
                                <input type="password" name="password_new" placeholder="Пароль">
                            </div>
                            <div class="remember-reestablish">
                                <div class="remember item">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">
                                            <span class="checkmark"></span></label>
                                    </div>
                                    <div class="char">
                                        <p class="add-info">Запомнить меня</p>
                                    </div>
                                </div>
                                <div class="reestablish item">
                                    <a href="/password_recovery.php">Восстановить пароль</a>
                                </div>
                            </div>
                            <div class="but-btn login-btn">
                                <button>Войти</button>
                            </div>
                        </form>
                        <div class="block_no_registration">
                            <p>У Вас еще нет учетной записи?</p>
                            <a href="/register-client.php">Регистрация клиента</a> <br>
                            <a href="/register-provider.php">Регистрация поставщика услуг</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/login/validateController.js"></script>
<?php include("template/footer.php") ?>

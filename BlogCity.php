<?php include("template/header-main.php") ?>

<div class="section-break">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="break-menu">
                    <a class="back" href="/">Главная </a><span>|</span>
                    <a class="back" href="/blog.php">Блог </a><span>|</span>
                    <a class="back" href="/"> Переезд </a><span>|</span>
                    <p>Название города</p>
                </div>
                <div class="title">
                    <h2 data-aos="zoom-out" data-aos-delay="100" data-aos-once="true">Переезд в Берлин</h2>
                    <div class="line"></div>
                    <div data-aos="zoom-out" data-aos-delay="100" data-aos-once="true" class="main-info">
                        <p>Планируете переезд?
                        </p>
                        <p>Оставьте заявку для получения предложений
                        </p>

                    </div>
                </div>
                <div class="girl" data-aos="fade-right"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200" data-aos-once="true">
                    <img src="img/BlogCity/block1/girl3.png" alt="">
                </div>
            </div>
            <div class="mob_girl">
                <img src="img/BlogCity/block1/girl3.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="section-choice main-content-choice">
    <div class="section-choice-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="choice-tab">
                        <div class="tab-menu">
                            <button class="filter active">
                                Переезд
                            </button>
                            <button class="filter">
                                Хранение вещей
                            </button>
                        </div>
                        <div class="tab-content">
                            <div class="content-choice">
                                <form class="main_relocation">
                                    <input type="hidden" name="val" value="Переезд">
                                    <div class="form-group ad blog-tabs-ad">
                                        <div class="adress-from">
                                            <p>Откуда</p>
                                            <input class="popup-index" type="tel" name="index_from" placeholder="Индекс"
                                                   maxlength="5">
                                        </div>
                                        <div class="adress-for">
                                            <p>Куда</p>
                                            <input class="popup-index" type="tel" name="index_for" placeholder="Индекс"
                                                   maxlength="5">
                                        </div>
                                        <div class="but-btn">
                                            <button type="submit" class="form-btn">Оставить заявку</button>
                                        </div>
                                    </div>
                                </form>
                                <p class="proposition">* Предложения по Вашей заявке предоставляются бесплатно и ни к
                                    чему не обязывают</p>
                            </div>
                            <div class="content-choice">
                                <form class="main_storage">
                                    <input type="hidden" name="val" value="Хранение вещей">
                                    <div class="form-group blog-tabs-ad">
                                        <div class="adress_storage">
                                            <p>Адрес</p>
                                            <input class="popup-index" type="tel" name="index_adress"
                                                   placeholder="Индекс" maxlength="5">
                                        </div>
                                        <p>Период хранения</p>
                                        <div class="cal-val">
                                            <input class="popup-calendar datepicker_from" name="date_from"
                                                   placeholder="дд.мм.гггг" type="text">
                                        </div>
                                        <div class="line-cal"></div>
                                        <div class="cal-val for_val">
                                            <input class="popup-calendar  datepicker_for" name="date_for"
                                                   placeholder="дд.мм.гггг" type="text">
                                        </div>
                                        <div class="but-btn">
                                            <button type="submit" class="form-btn"><span>Оставить заявку</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <p class="proposition">* Предложения по Вашей заявке предоставляются бесплатно и ни к
                                    чему не обязывают</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-constructor">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="constructor">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                            tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam
                            fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien
                            nunc accuan eget.</p>
                        <h3>Heading 3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                            tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam
                            fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien
                            nunc accuan eget.</p>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                            tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam
                            fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien
                            nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                            bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                            Proin
                            sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur
                            ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                            rhoncus pronin sapien nunc accuan eget.</p>
                        <h4>Heading 4</h4>
                        <ul>
                            <li><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></li>
                            <li><span>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</span>
                            </li>
                            <li><span>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span>
                            </li>
                            <li><span>Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span>
                            </li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                            tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam
                            fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien
                            nunc accuan eget.</p>

                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                            tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam
                            fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien
                            nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                            bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                            Proin
                            sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur
                            ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                            rhoncus pronin sapien nunc accuan eget.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="share">
                        <p>Поделиться:</p>
                        <div class="pluso" data-background="#ebebeb"
                             data-options="medium,round,line,horizontal,counter,theme=04"
                             data-services="twitter,facebook,google,linkedin,pinterest"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="section-helpful-information">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h2>Полезная информация</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a href="#">
                    <div class="relocation repeat-col useful-blog">
                        <div class="foto-relocation">
                                <img src="img/work/block5/foto1.png" alt="">
                        </div>
                        <div class="descript">
                            <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                                переезда</a>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="#">
                    <div class="relocation repeat-col useful-blog">
                        <div class="foto-relocation">
                                <img src="img/work/block5/foto2.png" alt="">
                        </div>
                        <div class="descript">
                            <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                                переезда</a>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="#">
                    <div class="relocation repeat-col useful-blog">
                        <div class="foto-relocation">
                                <img src="img/work/block5/foto3.png" alt="">
                        </div>
                        <div class="descript">
                            <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                                переезда</a>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


    <script type="text/javascript" src="/js/BlogCity.js"></script>
    <?php include("template/popUps.php") ?>
    <?php include("template/footer.php") ?>

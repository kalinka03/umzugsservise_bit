<?php include("template/header-lk.php") ?>


<div class="section-lk-top">
    <div class="section-ordering-services">
        <div class="container">
            <div class="row">
                <div class="order-relocation">
                    <h3>Переезд</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myTab" class="section-choice">
    <div class="section-steps-relocation">
        <div class="container">
            <div class="row">
                <div class="steps">
                    <div class="step-menu nav-tabs">
                        <button disabled class="filter active">
                            География переезда
                        </button>
                        <button disabled class="filter">
                            Условия обслуживания
                        </button>

                        <button disabled class="filter">
                            Список вещей
                        </button>

                        <button disabled class="filter not-active-step">
                            Контактные данные
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form id="#form_lk_relocation">
                    <div class="step-content">
                        <div class="content-choice active">
                            <div class="geography-block">
                                <div class="order-lk form-group form-all">
                                    <p>Укажите необходимый адрес перемещения Вашего груза и все сопутствующие параметры
                                        и мы
                                        сможем подобрать для Вас лучшие предложения.</p>
                                    <h4>Откуда</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="adress-lk">
                                            <div class="v-d-index index">
                                                <input  class="index pageRequired change_index" type="tel" name="index"
                                                       placeholder="Индекс"
                                                       maxlength="5">
                                                <span id="valid"></span>
                                            </div>
                                            <div class="v-d-city">
                                                <input class="city pageRequired name_city_from_index" type="text" name="city"
                                                       placeholder="Город" value="">
                                                <span id="valid"></span>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <input class="adress-form pageRequired" type="text" name="adress"
                                                   placeholder="Адрес">
                                            <span id="valid"></span>
                                        </div>
                                    </div>
                                    <h4>Куда</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="adress-lk">
                                            <div class="v-d-index index">
                                                <input  class="index pageRequired change_index_for" type="tel" name="index_for"
                                                       placeholder="Индекс"
                                                       maxlength="5">
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                            <div class="v-d-city">
                                                <input class="city pageRequired name_city_for_index" type="text" name="city_for"
                                                       placeholder="Город">
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <input class="adress-form pageRequired" type="text" name="adress_for"
                                                   placeholder="Адрес">
                                            <label class="warning" for="last_name">Заполните</label>
                                        </div>
                                    </div>
                                    <div class="line-black"></div>
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Сроки выполнения заказа:</label>
                                                <select class="change_order" name="pay_order">
                                                    <option selected disabled>Укажите сроки выполнения заказа</option>
                                                    <option value='1'>Срочно</option>
                                                    <option value='2'>Точная дата</option>
                                                    <option value='3'>Диапазон дат</option>
                                                </select>
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                            <div class="exact-date">
                                                <input class="sk_calendar popup-calendar datepicker_for pageRequired"
                                                       name="date_from"
                                                       placeholder="дд.мм.гггг" type="text">
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                            <div class="date-range">
                                                <input id="date_range" class="sk_calendar popup-calendar pageRequired"
                                                       name="date_range" readonly
                                                       placeholder="дд.мм.гггг" type="text">
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Площадь помещения м<sup>2</sup>:</label>
                                                <input class="adress-form pageRequired" type="tel" name="area"
                                                       placeholder="Площадь помещения">
                                                <label class="warning" for="last_name">Заполните</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <label class="all-label">Способ оплаты:</label>
                                            <select name="order">
                                                <option selected disabled>Укажите кто будет оплачивать заказ</option>
                                                <option>Частное лицо</option>
                                                <option>Государство</option>
                                                <option>Работодатель</option>
                                            </select>
                                        </div>
                                        <div class="lk-rel">
                                            <label class="all-label"> Комментарий к услуге:</label>
                                            <textarea class="pageRequired"  name="commit"
                                                      placeholder="Комментарий"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="but-btn  orderr_lk_btn  center-but btnNext">
                                    <button>Далее</button>
                                </div>
                            </div>
                        </div>
                        <div class="content-choice two-st">
                            <div class="conditions-block">
                                <div class="order-lk form-group form-all">
                                    <p>Укажите необходимый адрес перемещения Вашего груза и все сопутствующие параметры
                                        и мы
                                        сможем подобрать для Вас лучшие предложения.</p>
                                    <h4>Откуда</h4>
                                    <div class="all-profile lk-rel col-three">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Тип помещения:</label>
                                                <select name="room_type">
                                                    <option selected disabled>Укажите тип помещения</option>
                                                    <option>Квартира</option>
                                                    <option>Дом</option>
                                                    <option>Комната</option>
                                                    <option>Офис</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Расстояние до парковки, м:</label>
                                                <input class="adress-form" type="tel" name="distance"
                                                       placeholder="Расстояние до парковки">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Лифт:</label>
                                                <select name="elevator">
                                                    <option selected disabled>Укажите наличие лифта</option>
                                                    <option>Есть</option>
                                                    <option>Нет</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Этажей:</label>
                                                <input class="adress-form" type="tel" name="number_floors"
                                                       placeholder="Укажите количество этажей">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Комнат:</label>
                                                <input class="adress-form" type="tel" name="number_room"
                                                       placeholder="Укажите количество комнат">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Жильцов:</label>
                                                <input class="adress-form" type="tel" name="number_people"
                                                       placeholder="Укажите количество жильцов">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Необходимость установки знаков временного
                                                    ограничения запрета стоянки транспортных средств:</label>
                                                <select name="transport">
                                                    <option selected disabled>Выбрать</option>
                                                    <option>Нет</option>
                                                    <option>Да</option>
                                                    <option>Не знаю</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Куда</h4>
                                    <div class="all-profile lk-rel col-three">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Тип помещения:</label>
                                                <select name="r_type">
                                                    <option selected disabled>Укажите тип помещения</option>
                                                    <option>Квартира</option>
                                                    <option>Дом</option>
                                                    <option>Комната</option>
                                                    <option>Офис</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Расстояние до парковки, м:</label>
                                                <input class="adress-form" type="tel" name="distance_parking"
                                                       placeholder="Расстояние до парковки">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Лифт:</label>
                                                <select name="presence_elevator">
                                                    <option selected disabled>Укажите наличие лифта</option>
                                                    <option>Есть</option>
                                                    <option>Нет</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Этажей:</label>
                                                <input class="adress-form" type="tel" name="n_floor"
                                                       placeholder="Укажите количество этажей">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Необходимость установки знаков временного
                                                    ограничения запрета стоянки транспортных средств:</label>
                                                <select name="means_transport">
                                                    <option selected disabled>Выбрать</option>
                                                    <option>Нет</option>
                                                    <option>Да</option>
                                                    <option>Не знаю</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="additional-services">
                                        <h4>Дополнительные услуги</h4>
                                        <div class="checkbox">
                                            <label><input class="add_service" type="checkbox" value="">
                                                <span class="checkmark"></span></label>
                                        </div>
                                    </div>
                                    <p class="add-info">Если необходимы дополнительные услуги, укажите их
                                        пожалуйста.</p>
                                    <div class="hover-service">
                                        <div class="block-add-service">
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Упаковочные коробки</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Демонтаж мебели</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Сборка кухни</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Услуги водопроводчика</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Упаковать вещи</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Сборка мебели</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Столярные работы</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Подъемник</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Распаковать вещи</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Демонтаж кухни</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Услуги электрика</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-serv-descript">
                                        <div class="r-all">
                                            <label class="all-label">Другое:</label>
                                            <textarea class="popup-picking__text" name="commit"
                                                      placeholder="Опишите что нужно сделать"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-steps">
                                    <div class="but-btn orderr_lk_btn btnPrevious">
                                        <button>Назад</button>
                                    </div>
                                    <div class="but-btn orderr_lk_btn btnNext">
                                        <button>Далее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-choice three-st">
                            <div class="list-things-block">
                                <div class="form-group form-all choice-block-things">
                                    <h4>Составьте инвентаризацию своих вещей</h4>
                                    <div class="step-things-tree-block"></div>
                                    <p class="js_things_info"> Составление списка вещей способствует более точной оценке
                                       стоимости и оптимизации
                                       процесса переезда</p>
                                    <p class="js_things_info">
                                        Выберите любой удобный способ предоставления списка вещей для оценки и
                                        предоставления выгодного предложения.</p>
                                    <div class="downloading-files">
                                        <div class="list">
                                            <div class="item create_list_js">
                                                <div class="left-col document">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Создать список онлайн</h3>
                                                            <p>Найдите и добавьте вещи, которые необходимо перевезти</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col">
                                                        <div id="1" class="list-things-bt create_list_js">
                                                            <button>Создать список</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item open_foto">
                                                <div class="left-col foto">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить фото</h3>
                                                            <p>Объем фото не должен превышать 5 Mb</p>
                                                        </div>
                                                    </div>
                                                    <div class="right-col">
                                                        <div id="2" class="list-things-bt open_foto">
                                                            <button>Прикрепить фото</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-foto arhive-foto">
                                                <div class="foto-block tab2_foto">
                                                    <div class="new-fot add-plus">
                                                        <label for="uploadbtn" class="uploadButton"></label>
                                                        <input style="opacity: 0; z-index: -1;" type="file"
                                                               name="upload" id="uploadbtn">
                                                    </div>
                                                    <div class="imgg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item open_download_file">
                                                <div class="left-col things">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить готовый список вещей</h3>
                                                            <p>Можно прикрепить файлы формата pdf/doc/xls.</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col file_">
                                                        <div id="3" class="list-things-bt open_download_file">
                                                            <div class="file-upload">
                                                                <label>
                                                                    <input type="file" name="file">
                                                                    <span>Прикрепить документ</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-file">
                                                <div class="file-block">
                                                    <p>Загружен файл</p>
                                                    <div class="fileDownload">
                                                        <input id="filename" class="filename"> </input>
                                                        <div class="del-file"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn  orderr_lk_btn btnPrevious">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  orderr_lk_btn choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="create-things">
                                    <p>Составление списка вещей способствует оптимизации переезда.</p>
                                    <p> Найдите и добавьте вещи для каждого имеющегося у Вас помещения.</p>
                                    <div class="catalog-things">
                                        <div class="choise-catalog-things">
                                            <div class="tab-menu-things">
                                                <button class="filter active">
                                                    Гостинная
                                                </button>
                                                <button class="filter">
                                                    Спальня
                                                </button>
                                                <button class="filter">
                                                    Столовая
                                                </button>
                                                <button class="filter">
                                                    Кухня
                                                </button>
                                                <button class="filter">
                                                    Ванная комната
                                                </button>
                                                <button class="filter">
                                                    Прихожая
                                                </button>
                                                <button class="filter">
                                                    Офис
                                                </button>
                                                <button class="filter">
                                                    Балкон/Мансарда/Терраса
                                                </button>
                                                <button class="filter">
                                                    Гараж/Подвал/Чердак
                                                </button>
                                                <button class="filter">
                                                    Кладовая
                                                </button>
                                                <button class="filter">
                                                    Другое
                                                </button>
                                            </div>
                                            <div class="tab-content-things">
                                                <div class="content-choice-things" data="living_room" data-gallery="living_room">
                                                    <div class="nameThings">
                                                        <h3>Гостинная</h3>
                                                    </div>
                                                    <div class="addThings untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <input id="things_filter_show" type="text" placeholder="Поиск">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="bedroom">
                                                    <div class="nameThings">
                                                        <h3>Спальня</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="canteen">
                                                    <div class="nameThings">
                                                        <h3>Столовая</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="kitchen">
                                                    <div class="nameThings">
                                                        <h3>Кухня</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="bathroom">
                                                    <div class="nameThings">
                                                        <h3>Ванная комната</h3>
                                                    </div>
                                                    <div class="addThings untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="hallway">
                                                    <div class="nameThings">
                                                        <h3>Прихожая</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="office">
                                                    <div class="nameThings">
                                                        <h3>Офис</h3>
                                                    </div>
                                                    <div class="addThings untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="balcony">
                                                    <div class="nameThings">
                                                        <h3>Балкон/Мансарда/Терраса
                                                        </h3>
                                                    </div>
                                                    <div class="addThings untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="garage">
                                                    <div class="nameThings">
                                                        <h3>Гараж/Подвал/Чердак</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="pantry">
                                                    <div class="nameThings">
                                                        <h3>Кладовая</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things" data="other">
                                                    <div class="nameThings">
                                                        <h3>Другое</h3>
                                                    </div>
                                                    <div class="addThings  untitled">
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn  orderr_lk_btn catalogEngine">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  orderr_lk_btn choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="check-list complete_checking_things">
                                    <div class="form-group form-all">
                                        <h4>Полный список Ваших вещей для переезда</h4>
                                        <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо перевезти.
                                            Сумма предложения от исполнителя формируется на основании этого списка.
                                            В случае, если объем или количество вещей будут отличаться, это может
                                            повлиять
                                            на конечную стоимость услуги.</p>
                                    </div>
                                    <div class="completed_things">
                                        </div>
                                        <div class="all-quantity-things">
                                            <input type="hidden" id="area_things">
                                            <h2>Всего <span class="quantity-things"></span> <span>м<sup>3</sup></span></h2>
                                        </div>
                                        <div class="arrow-steps">
                                            <div class="but-btn  orderr_lk_btn btnChangeThings">
                                                <button>Изменить</button>
                                            </div>
                                            <div class="but-btn orderr_lk_btn  btnNext submission-btn">
                                                <button>Далее</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="check-list full_photo_check">
                                        <div class="form-group form-all">
                                            <h4>Полный список Ваших вещей для переезда</h4>
                                            <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо
                                                перевезти.
                                                Сумма предложения от исполнителя формируется на основании этого списка.
                                                В случае, если объем или количество вещей будут отличаться, это может
                                                повлиять
                                                на конечную стоимость услуги.</p>
                                        </div>
                                        <div class="completed_things">
                                            <h2>Загруженные фото</h2>
                                            <div class="list-things">
                                                <div class="compl_foto">
                                                </div>
                                            </div>
                                            <div class="line_shadow"></div>
                                        </div>
                                        <div class="arrow-steps">
                                            <div class="but-btn orderr_lk_btn  btnChangeThings">
                                                <button>Изменить</button>
                                            </div>
                                            <div class="but-btn orderr_lk_btn  btnNext choice_paragraph submission-btn">
                                                <button>Далее</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="check-list full_download_check">
                                        <div class="form-group form-all">
                                            <h4>Полный список Ваших вещей для переезда</h4>
                                            <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо
                                                перевезти.
                                                Сумма предложения от исполнителя формируется на основании этого списка.
                                                В случае, если объем или количество вещей будут отличаться, это может
                                                повлиять
                                                на конечную стоимость услуги.</p>
                                        </div>
                                        <div class="completed_things">
                                            <h2>Готовый список вещей</h2>
                                            <div class="all-download-file">
                                                <div class="picture-document">
                                                </div>
                                                <div class="download_file">
                                                </div>
                                            </div>
                                            <div class="line_shadow"></div>
                                        </div>

                                        <div class="arrow-steps">
                                            <div class="but-btn  orderr_lk_btn btnChangeThings">
                                                <button>Изменить</button>
                                            </div>
                                            <div class="but-btn  orderr_lk_btn btnNext choice_paragraph submission-btn">
                                                <button>Далее</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-choice four-st">
                                <div class="center-block">
                                    <div class="geography-block">
                                        <div class="order-lk form-group form-all">
                                            <p>Укажите, пожалуйста, Ваши контактные данные. В ближайшее время на адрес
                                                Вашей электронной почты будет отправлено письмо со ссылкой, по которой
                                                Вы сможете просмотреть полученные предложения.</p>
                                            <h4>Укажите Ваши контактные данные</h4>
                                            <div class="recourse-lk contact-things">
                                                <div class="recourse-name">
                                                    <div class="item">
                                                        <label>
                                                            <input class="radio" type="radio" name="radio-test"
                                                                   value="Господин" checked>
                                                            <span class="radio-custom"></span>
                                                            <span class="label">Господин</span>
                                                        </label>
                                                    </div>
                                                    <div class="item">
                                                        <label>
                                                            <input class="radio" type="radio" name="radio-test"
                                                                   value="Госпожа">
                                                            <span class="radio-custom"></span>
                                                            <span class="label">Госпожа</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-profile lk-rel">
                                                <div class="cont-things-form">
                                                    <div class="item">
                                                        <input type="text" name="name" placeholder="Ваше имя">
                                                    </div>
                                                    <div class="item">
                                                        <input type="text" name="surname" placeholder="Фамилия">
                                                    </div>
                                                    <div class="item">
                                                        <input type="text" name="email" placeholder="E-mail">
                                                    </div>
                                                    <div class="item">
                                                        <input class="form-control" placeholder="Телефон" name="phone"
                                                               id="Piramida_phone" type="tel" maxlength="20"/>
                                                        <div class="help-block error" id="Piramida_phone_em_"
                                                             style="display:none"></div>
                                                        <input type="checkbox" id="phone_mask" placeholder="Телефон"
                                                               checked>
                                                        <label id="descr" for="phone_mask" class="phone-mask">Использовать
                                                            маску</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="arrow-steps">
                                            <div class="but-btn orderr_lk_btn  btnPrevious">
                                                <button>Назад</button>
                                            </div>
                                            <div class="but-btn orderr_lk_btn  btnSubmit">
                                                <button>Отправить заявку</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/lk_order/mainController.js"></script>
<script type="text/javascript" src="/js/lk_order/tabsController.js"></script>
<script type="text/javascript" src="/js/lk_order/thirdTabsEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/tabListEngine.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/uploadPhoto.js"></script>
<script type="text/javascript" src="/js/lk_order/validateController.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateEngineEvents.js"></script>
<script type="text/javascript" src="/js/lk_order/showListThings.js"></script>
<script type="text/javascript" src="/js/lk_order/listPhotoTab2.js"></script>
<script type="text/javascript" src="/js/lk_other/filterThings/filterOneThings.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateOtherEngineEvents.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/PhotoGalleryEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/areaThings.js"></script>
<script type="text/javascript" src="/js/lk_other/filterThings/filterThings.js"></script>
<script type="text/javascript" src="/js/lk_order/checkingDimensions.js"></script>
<script type="text/javascript" src="/js/lk_order/eventNewThing.js"></script>
<script type="text/javascript" src="/js/lk_order/arrayThings.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLTemplates.js"></script>
<script type="text/javascript" src="/js/system/indexCity.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLThings.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLOtherThings.js"></script>
<script type="text/javascript" src="/js/lk_other/validateNumber.js"></script>
<script type="text/javascript" src="/js/lk_order/nexTab.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/AddThingsOneArea.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/AddThingsOtherArea.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

<div id="js_сancel_application">
    <div class="answer-title">
        <p>Вы действительно хотите отменить заявку? Данная заявка будет деактивирована и переведена в архив.</p>
        <div class="action-btn">
            <div class="bt-white">
                <form>
                    <button>Да</button>
                </form>
            </div>
            <div class="bt-white refuse_offer_close">
                <button>Нет</button>
            </div>
        </div>
    </div>
</div>


<div id="js_reason_date">
    <div class="answer-title">
        <p>Текст причины изменения срока выполнения заказа, указанный компанией-исполнителем, предложение которой было подтверждено клиентом.</p>
       <p>Текст причины изменения срока выполнения заказа, указанный компанией-исполнителем, предложение которой было подтверждено клиентом.</p>
    <p>Текст причины изменения срока выполнения заказа, указанный компанией-исполнителем, предложение которой было подтверждено клиентом.</p>
    </div>
</div>


<div id="js_confirm_offer">
    <div class="answer-title">
        <p>Подтверждая предложение <span class="js_confirm_offer_name_company">Название фирмы</span>по заявке ID <span>123456</span>, Вы принимаете это предложение, как окончательное</p>
        <div class="action-btn">
            <div class="bt-white">
                <form>
                    <button>Да</button>
                </form>

            </div>
            <div class="bt-white refuse_offer_close">
                <button>Нет</button>
            </div>
        </div>
    </div>
</div>

<div id="js_refuse_offer">
    <div class="answer-title">
        <p>Вы действительно хотите отказаться от этого предложения? Данная позиция будет удалена из списка.</p>
        <div class="action-btn">
            <div class="bt-white">
                <form>
                    <button>Да</button>
                </form>
            </div>
            <div class="bt-white refuse_offer_close">
                <button>Нет</button>
            </div>
        </div>
    </div>
</div>

<div id="js_come_contact">
    <div class="answer-title">
        <p class="backg">
            Ваш запрос отправлен <span>Название фирмы.</span> Сотрудник компании свяжется с Вами в ближайшее время.</p>
    </div>
</div>

<div id="js_map">
    <div id="google_map">
    </div>
</div>
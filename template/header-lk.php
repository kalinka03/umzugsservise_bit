<?php include("template/header.php") ?>
    <div class="section-header-lk">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-8 col-xs-5 p-l p-r">
                    <div class="picture-logo">
                        <a href="/">
                            <img src="img/lk_client/block1/logo.svg" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-3 col-xs-3">
                    <div class="lk-top-all">
                        <input class="checkbox-toggle" type="checkbox"/>
                        <div class="hamburger">
                            <div></div>
                        </div>
                        <div class="hover-mob">
                            <div>
                                <div>
                                    <div class="lk-parner">
                                        <a href="/lk_client.php">Главная страница ЛК</a>
                                    </div>
                                    <div class="lk-parner profile">
                                        <a href="/lk_client_profile.php">Профиль</a>
                                    </div>
                                    <div class="qustion-but">
                                        <div class="bt-white btn-lk-m">
                                            <a href="#js-qustion">Задать вопрос</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-3  col-xs-4">
                    <div class="entrance">
                        <div class="language">
                            <a href="#">
                                <img src="img/header/1_menu_Flag_of_United_Kingdom.svg" alt="">
                            </a>
                            <a href="#">
                                <img src="img/header/1_menu_Flag_of_Germany.svg" alt="">
                            </a>
                            <a class="active" href="#">
                                <img src="img/header/1_menu_Flag_of_Russia.svg" alt="">
                            </a>
                        </div>
                        <div class="header-btn-lk">
                            <a href="#">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
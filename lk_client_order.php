<?php include("template/header-lk.php") ?>
<div class="section-lk-top">
    <div class="container">
        <div class="top">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12 p-l p-r">
                    <div class="name-person">
                        <div class="all">
                            <h4>Дюсельдорф Фонмюллеркренсербернгмингер</h4>
                            <p>40219 Бад-Мюнстер-на-Штайн-Эбернбурге </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <div class="but-btn but-lk">
                        <a href="/order.php">Оставить заявку</a>
                    </div>
                </div>
                <div class="all-demens">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 not-pad">
                        <div class="btn-black btn-dimensions my_application">
                            <a href="/lk_client_list.php">Мои заявки</a>
                        </div>
                        <a href="#" class="red-circle"></a>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 not-pad">
                        <div class="btn-black btn-dimensions">
                            <a href="#">Архив заявок</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-sm-12">
            <div class="come_back">
                <a href="/lk_client_list.php">Назад</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-news">
            <div class="col-sm-12">
                <div class="application_top">
                    <div class="col-sm-6 col-xs-12">
                        <div class="all-applicat">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="#">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="action-btn">
                                <div class="bt-white btn-lk-m change_application_my">
                                    <a href="#">Изменить</a>
                                </div>
                                <div class="bt-white save_change_application">
                                    <button class="accepted">Сохранить</button>
                                </div>
                                <div class="bt-white btn-lk-m">
                                    <a class="сancel_application" href="#js_сancel_application">Отменить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-col">
                            <div class="suggestion confirmed-proposal">
                                <p>Предложение подтверждено</p>
                            </div>
                            <div class="application_canceled">
                                <p>Заявка отменена</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <form>
                <div class="choice-tab-list-order">
                    <div class="tab-menu">
                        <button class="filter active">
                            Описание
                        </button>
                        <button class="filter sentence">
                            Предложения
                        </button>
                    </div>
                    <div class="tab-content">
                        <div class="content-choice">
                            <div class="application-descript-top">
                                <div class="col-sm-6 col-sm-push-6 col-xs-12 r-pad offer-action">
                                    <div class="col-right">
                                        <div class="bid-name">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>3584102</span></p>
                                            </div>
                                        </div>
                                        <div class="notification">
                                            <p>уведомляет об изменении даты выполнения заказа на дд.мм.гггг по
                                                причине:</p>
                                        </div>
                                        <div class="cause">
                                            <a href="#js_reason_date">Причина изменения даты</a>
                                        </div>
                                        <div class="offer">
                                            <div class="action-btn">
                                                <div class="bt-white">
                                                    <button class="accepted">Принять</button>
                                                </div>
                                                <div class="bt-white">
                                                    <button class="reject">Отклонить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-push-6 col-xs-12 r-pad proposal-accepted">
                                    <div class="col-right">
                                        <div class="bid-name">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании </a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>3584102</span></p>
                                            </div>
                                        </div>
                                        <div class="notification">
                                            <p>уведомляет об изменении даты выполнения заказа на дд.мм.гггг по
                                                причине:</p>
                                        </div>
                                        <div class="cause">
                                            <a href="#js_reason_date">Причина изменения даты</a>
                                        </div>
                                        <div class="offer">
                                            <p>Предложение принято</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-push-6 col-xs-12 r-pad offer-rejected">
                                    <div class="col-right">
                                        <div class="bid-name">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>3584102</span></p>
                                            </div>
                                        </div>
                                        <div class="notification">
                                            <p>уведомляет об изменении даты выполнения заказа на дд.мм.гггг по
                                                причине:</p>
                                        </div>
                                        <div class="cause">
                                            <a href="#js_reason_date">Причина изменения даты</a>
                                        </div>
                                        <div class="offer">
                                            <p>Предложение отклонено</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-pull-6 col-xs-12">
                                    <div class="descript">
                                        <div class="list">
                                            <div class="name">
                                                <p>Откуда:</p>
                                            </div>
                                            <div class="text">
                                                <p>50674 <span class="found_lg_city_from"> Кельн </span> Германия +
                                                    адрес</p>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="name">
                                                <p>Куда:</p>
                                            </div>
                                            <div class="text">
                                                <p>60329 <span class="found_lg_city_for"> Франкфурт-на-Майне </span>
                                                    Unter den Linden 63-65</p>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="name">
                                                <p>Расстояние:</p>
                                            </div>
                                            <div class="text">
                                                <p>Около <span class="distance"> </span> км (по прямой) <a
                                                            class="map_city" href="#js_map"><img
                                                                src="img/lk_list/map.svg" alt=""> </a></p>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="name">
                                                <p>Дата выполнения заказа:</p>
                                            </div>
                                            <div class="text">
                                                <p class="date_application_form">дд.мм.гггг</p>
                                                <input class="sk_calendar popup-calendar datepicker_for pageRequired"
                                                       name="date_from"
                                                       placeholder="дд.мм.гггг" value="дд.мм.гггг" type="text">
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="name">
                                                <p>Площадь помещения:</p>
                                            </div>
                                            <div class="text">
                                                <p>62 м<sup>2</sup></p>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="name">
                                                <p>Способ оплаты:</p>
                                            </div>
                                            <div class="text">
                                                <p>частное лицо</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>Комментарий к услуге:</h4>
                                <p class="commit_send_application">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                                <p class="commit_send_application">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget. Lorem ipsum
                                    dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                                    gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus
                                    mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                                <textarea class="commit_send_company_application">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                   </textarea>
                                <a class="open_param" href="#">Отобразить дополнительные параметры</a>
                                <div class="conditions_moving">
                                    <div class="title">
                                        <h4>Условия переезда</h4>
                                    </div>
                                    <div class="all-block">
                                        <div class="l-col">
                                            <h5>Откуда</h5>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Тип помещения:</p>
                                                </div>
                                                <div class="text">
                                                    <p>Квартира</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Этажей:</p>
                                                </div>
                                                <div class="text">
                                                    <p>5</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Комнат:</p>
                                                </div>
                                                <div class="text">
                                                    <p>2</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Жильцов:</p>
                                                </div>
                                                <div class="text">
                                                    <p>2</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Лифт:</p>
                                                </div>
                                                <div class="text">
                                                    <p>есть</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Расстояние до парковки:</p>
                                                </div>
                                                <div class="text">
                                                    <p>20 м</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Необходимость установки знаков временного ограничения запрета
                                                        стоянки транспортных средств</p>
                                                </div>
                                                <div class="text">
                                                    <p>не знаю</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="r-col">
                                            <h5>Куда</h5>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Тип помещения:</p>
                                                </div>
                                                <div class="text">
                                                    <p>Квартира</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Этажей:</p>
                                                </div>
                                                <div class="text">
                                                    <p>5</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Лифт:</p>
                                                </div>
                                                <div class="text">
                                                    <p>есть</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Расстояние до парковки:</p>
                                                </div>
                                                <div class="text">
                                                    <p>20 м</p>
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="name">
                                                    <p>Необходимость установки знаков временного ограничения запрета
                                                        стоянки транспортных средств</p>
                                                </div>
                                                <div class="text">
                                                    <p>не знаю</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide_list">
                                        <a href="#">Скрыть</a>
                                    </div>
                                </div>
                                <h4>Дополнительные услуги:</h4>
                                <ul>
                                    <li><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></li>
                                    <li><span>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</span>
                                    </li>
                                    <li><span>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span>
                                    </li>
                                    <li><span>Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span>
                                    </li>
                                </ul>
                                <p class="other_list_order">Другое</p>
                                <p class="other_list_order_descript">Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                                    et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque
                                    penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum,
                                    nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc
                                    eget.</p>
                                <h4 class="all-things-completed">Список вещей:</h4>
                                <div class="line_shadow"></div>
                                <div class="list_all_things">
                                    <div class="completed_things all_server_things">
                                        <div class="list_all_js">
                                            <h2>Гостинная</h2>
                                            <div class="list-things completed_list_things">
                                                <div class="item">
                                                    <div class="l-col">
                                                        <img src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                             alt="">
                                                    </div>
                                                    <div class="center-col">
                                                        <h5> Книжная полка <span>x 5</span></h5>
                                                        <input type="hidden" name="list_area" value="">
                                                        <p class="parametr_specify">0,86 </p><span>м<sup>3</sup></span>
                                                        <p class="parametr_all_specify">1х1х2<span>м</span></p>
                                                    </div>
                                                    <div class="r-col">
                                                        <p>Комментарий</p>
                                                        <div class="gallery-foto">
                                                            <a href="#js_popup_foto_things_application">Смотреть фото</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="line_shadow"></div>
                                        </div>

                                    <div class="list_all_js">
                                        <h2>Гостинная</h2>
                                        <div class="list-things completed_list_things">
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5> Книжная полка <span>x 5</span></h5>
                                                    <input type="hidden" name="list_area" value="">
                                                    <p class="parametr_specify">0,86 </p><span>м<sup>3</sup></span>
                                                    <p class="parametr_all_specify">1х1х2<span>м</span></p>
                                                </div>
                                                <div class="r-col">
                                                    <p>Комментарий</p>
                                                    <div class="gallery-foto">
                                                        <a href="#js_popup_foto_things_application">Смотреть фото</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line_shadow"></div>
                                    </div>
                                    <div class="list_all_js">
                                        <h2>Прихожая</h2>
                                        <div class="list-things completed_list_things">
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/18Zaglushka.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5> Подставка для зонтов <span>x 5</span></h5>
                                                    <input type="hidden" name="list_area" value="">
                                                    <p class="parametr_specify">0,86 </p><span>м<sup>3</sup></span>
                                                </div>
                                                <div class="r-col">
                                                    <p>Комментарий</p>
                                                    <div class="gallery-foto">
                                                        <a href="#js_popup_foto_things_application">Смотреть фото</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line_shadow"></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="all-quantity-things completed_list_things">
                                    <h2>Всего <span class="quantity-things">32,68</span><span>м<sup>3</sup></span></h2>
                                </div>


                            </div>
                            <div class="col-sm-12">
                                <div class="completed_things all_server_things">
                                    <h2>Загруженные фото</h2>
                                    <div class="list-things">
                                        <div class="compl_foto">
                                            <a href="#js_popup_foto_things_downloaded">
                                            <div class="item">
                                                <img src="img/lk_list/block1/img1.png" alt="">
                                            </div>
                                            </a>
                                            <a href="#js_popup_foto_things_downloaded">
                                            <div class="item">
                                                <img src="img/lk_list/block1/img1.png" alt="">
                                            </div>
                                            </a>
                                            <a href="#js_popup_foto_things_downloaded">
                                            <div class="item">
                                                <img src="img/lk_list/block1/img1.png" alt="">
                                            </div>
                                            </a>
                                            <a href="#js_popup_foto_things_downloaded">
                                            <div class="item">
                                                <img src="img/lk_list/block1/img1.png" alt="">
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="completed_things all_server_things">
                                    <h2>Готовый список вещей</h2>
                                    <div class="all-download-file">
                                        <div class="picture-document">
                                            <img src="img/order-relocation/tabs3/files/doc.svg" alt="">
                                        </div>
                                        <div class="download_file">
                                            Документ Microsoft Office Word.docx
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="content-choice">
                            <div class="executor_definition">
                                <p>Если Вы определились с исполнителем, обязательно подтвердите выбранное
                                    предложение</p>
                            </div>
                            <div class="block_how_it_works">
                                <a href="#">Как это работает</a>
                            </div>
                            <div class="hover_how_works">
                                <div class="close-block-work">
                                </div>
                                <p>На этой странице отображаются предложения по Вашей заявке. В каждом предложении Вы
                                    видите компанию-исполнителя, предложенную сумму выполнения заказа и две кнопки:
                                    отказаться т вступить в контакт. Также Вы можете запросить стоимость выполнения
                                    услуги у ближайших компаний-исполнителей.</p>
                                <div class="cause_work">
                                    <div class="but">
                                        <button>Отказаться</button>
                                    </div>
                                    <div class="info">
                                        <p>Если по какой-то причине предложение Вам не подходит, Вы можете от него
                                            отказаться. При подтверждении отказа данная позиция будет удалена из
                                            списка.</p>
                                    </div>
                                </div>
                                <div class="cause_work">
                                    <div class="but">
                                        <button>Вступить в контакт</button>
                                    </div>
                                    <div class="info">
                                        <p>Если предложение Вам подходит, Вы можете запросить контакт с
                                            компанией-исполнителем для уточнения деталей выполнения заказа. После
                                            нажатия этой кнопки, выбранной компании отравляется запрос, после чего с
                                            Вами свяжется сотрудник этой компании.</p>
                                    </div>
                                </div>
                                <div class="cause_work">
                                    <div class="but">
                                        <button>Запросить стоимость</button>
                                    </div>
                                    <div class="info">
                                        <p>Система отслеживает ближайшие по расположению компании, что дает возможность
                                            запросить стоимость выполнения услуги без ожидания предложений от других
                                            компаний.</p>
                                    </div>
                                </div>
                                <div class="cause_work">
                                    <div class="but">
                                        <button>Подтвердить предложение</button>
                                    </div>
                                    <div class="info">
                                        <p>Когда Вы определились с компанией-исполнителем, необходимо подтвердить это
                                            предложение. Это необходимо для отслеживания качества работы сервиса и
                                            соблюдения правил обслуживания.</p>
                                        <p class="last_confirmation">После подтверждения предложения возможность
                                            изменения заявки становится неактивна.</p>
                                    </div>
                                </div>
                                <div class="wish-success">
                                    <p>Желаем успехов!</p>
                                </div>
                            </div>

                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                    <div class="foto">
                                        <img src="img/lk_list/block1/img1.png" alt="">
                                    </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                <div class="about-company">
                                    <div class="company-prop">
                                        <div class="l-col">
                                            <a href="/lk_client_info_view_partner.php">Название компании</a>
                                                   </div>
                                        <div class="r-col">
                                            <p>ID <span>2510603</span></p>
                                        </div>
                                    </div>
                                    <p class="city_partner_company">40219 Dusseldorf</p>
                                </div>
                                    <div class="down_company_partner_all">
                                        <div class="reviews-company-prop">
                                            <div class="star-company">
                                                <div class="star">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="review-prop">
                                                <div class="quantity">
                                                    <p>Отзывов: <span>114</span></p>
                                                </div>
                                                <div class="percent">
                                                    <p>98<span>% положительных</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-company">
                                            <p>2077 <span>€</span></p>
                                        </div>
                                        <div class="choice-offer">
                                            <div class="but">
                                                <a class="refuse_offer" href="#js_refuse_offer">Отказаться</a>
                                            </div>
                                            <div class="but">
                                                <a class="to_come_contact" href="#js_come_contact">Вступить в контакт</a>
                                            </div>
                                        </div>
                                        <div class="confirm_offer after_send_form_confirmed">
                                            <p>Предложение принято, запрос отправлен</p>
                                            <div class="but">
                                                <a class="yes_confirm_offer" href="#js_confirm_offer">Подтвердить предложение</a>
                                            </div>
                                            <div class="right-col hide_confirmed-proposal">
                                                <div class="suggestion confirmed-proposal">
                                                    <p>Предложение подтверждено</p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                            </div>
                            </div>
                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                        <div class="foto">
                                            <img src="img/lk_list/block1/img1.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                    <div class="about-company">
                                        <div class="company-prop">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>2510603</span></p>
                                            </div>
                                        </div>
                                        <p class="city_partner_company">40219 Dusseldorf</p>
                                    </div>
                                    <div class="down_company_partner_all">
                                        <div class="reviews-company-prop">
                                            <div class="star-company">
                                                <div class="star">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="review-prop">
                                                <div class="quantity">
                                                    <p>Отзывов: <span>114</span></p>
                                                </div>
                                                <div class="percent">
                                                    <p>98<span>% положительных</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-company">
                                            <p>2077 <span>€</span></p>
                                        </div>
                                        <div class="choice-offer">
                                            <div class="but">
                                                <a class="refuse_offer" href="#js_refuse_offer">Отказаться</a>
                                            </div>
                                            <div class="but">
                                                <a class="to_come_contact" href="#js_come_contact">Вступить в контакт</a>
                                            </div>
                                        </div>
                                        <div class="confirm_offer after_send_form_confirmed">
                                            <p>Предложение принято, запрос отправлен</p>
                                            <div class="but">
                                                <a class="yes_confirm_offer" href="#js_confirm_offer">Подтвердить предложение</a>
                                            </div>
                                            <div class="right-col hide_confirmed-proposal">
                                                <div class="suggestion confirmed-proposal">
                                                    <p>Предложение подтверждено</p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                        <div class="foto">
                                            <img src="img/lk_list/block1/img1.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                    <div class="about-company">
                                        <div class="company-prop">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>2510603</span></p>
                                            </div>
                                        </div>
                                        <p class="city_partner_company">40219 Dusseldorf</p>
                                    </div>
                                    <div class="down_company_partner_all">
                                        <div class="reviews-company-prop">
                                            <div class="star-company">
                                                <div class="star">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="review-prop">
                                                <div class="quantity">
                                                    <p>Отзывов: <span>114</span></p>
                                                </div>
                                                <div class="percent">
                                                    <p>98<span>% положительных</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-company">
                                            <p>2077 <span>€</span></p>
                                        </div>
                                        <div class="choice-offer">
                                            <div class="but">
                                                <a class="refuse_offer" href="#js_refuse_offer">Отказаться</a>
                                            </div>
                                            <div class="but">
                                                <a class="to_come_contact" href="#js_come_contact">Вступить в контакт</a>
                                            </div>
                                        </div>
                                        <div class="confirm_offer after_send_form_confirmed">
                                            <p>Предложение принято, запрос отправлен</p>
                                            <div class="but">
                                                <a class="yes_confirm_offer" href="#js_confirm_offer">Подтвердить предложение</a>
                                            </div>
                                            <div class="right-col hide_confirmed-proposal">
                                                <div class="suggestion confirmed-proposal">
                                                    <p>Предложение подтверждено</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                    <div class="foto">
                                        <img src="img/lk_list/block1/img1.png" alt="">
                                    </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                <div class="about-company">
                                    <div class="company-prop">
                                        <div class="l-col">
                                            <a href="/lk_client_info_view_partner.php">Название компании</a>
                                        </div>
                                        <div class="r-col">
                                            <p>ID <span>2510603</span></p>
                                        </div>
                                    </div>
                                    <p class="city_partner_company">40219 Dusseldorf</p>
                                </div>
                                    <div class="down_company_partner_all">
                                    <div class="reviews-company-prop">
                                        <div class="star-company">
                                            <div class="star">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="review-prop">
                                            <div class="quantity">
                                                <p>Отзывов: <span>114</span></p>
                                            </div>
                                            <div class="percent">
                                                <p>98<span>% положительных</span></p>
                                            </div>
                                        </div>
                                    </div>
                                <div class="price-company">
                                </div>
                                <div class="request-price">
                                    <p class="request-sent">Запрос отправлен</p>
                                    <div class="but">
                                        <button>Запросить стоимость услуги</button>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>


                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                        <div class="foto">
                                            <img src="img/lk_list/block1/img1.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                    <div class="about-company">
                                        <div class="company-prop">
                                            <div class="l-col">
                                                <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            </div>
                                            <div class="r-col">
                                                <p>ID <span>2510603</span></p>
                                            </div>
                                        </div>
                                        <p class="city_partner_company">40219 Dusseldorf</p>
                                    </div>
                                    <div class="down_company_partner_all">
                                        <div class="reviews-company-prop">
                                            <div class="star-company">
                                                <div class="star">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="review-prop">
                                                <div class="quantity">
                                                    <p>Отзывов: <span>114</span></p>
                                                </div>
                                                <div class="percent">
                                                    <p>98<span>% положительных</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-company">
                                        </div>
                                        <div class="request-price">
                                            <p class="request-sent">Запрос отправлен</p>
                                            <div class="but">
                                                <button>Запросить стоимость услуги</button>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                    <div class="foto">
                                        <img src="img/lk_list/block1/img1.png" alt="">
                                    </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                <div class="about-company">
                                    <div class="company-prop">
                                        <a href="/lk_client_info_view_partner.php">
                                        <div class="l-col">
                                            <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            <p>40219 Dusseldorf</p>
                                        </div>
                                        </a>
                                        <div class="r-col">
                                            <p>ID <span>2510603</span></p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="down_company_partner_all">
                                    <div class="reviews-company-prop">
                                        <div class="star-company">
                                            <div class="star">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="review-prop">
                                            <div class="quantity">
                                                <p>Отзывов: <span>114</span></p>
                                            </div>
                                            <div class="percent">
                                                <p>98<span>% положительных</span></p>
                                            </div>
                                        </div>
                                    </div>

                                <div class="price-company confirmed-price">
                                    <p>2077 <span>€</span></p>
                                </div>
                                <div class="confirm_offer">
                                    <p>Предложение принято, запрос отправлен</p>
                                    <div class="but">
                                        <a class="yes_confirm_offer" href="#js_confirm_offer">Подтвердить предложение</a>
                                    </div>
                                    <div class="right-col hide_confirmed-proposal">
                                        <div class="suggestion confirmed-proposal">
                                            <p>Предложение подтверждено</p>
                                        </div>
                                    </div>

                                </div>
                                </div>
                                </div>
                            </div>













                            <div class="company_offer">
                                <div class="foto-company">
                                    <a href="/lk_client_info_view_partner.php">
                                    <div class="foto">
                                        <img src="img/lk_list/block1/img1.png" alt="">
                                    </div>
                                    </a>
                                </div>
                                <div class="all_block_company_partner">
                                <div class="about-company">
                                    <div class="company-prop">
                                        <div class="l-col">
                                            <a href="/lk_client_info_view_partner.php">Название компании</a>
                                            <p>40219 Dusseldorf</p>
                                        </div>
                                        <div class="r-col">
                                            <p>ID <span>2510603</span></p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="down_company_partner_all">
                                    <div class="reviews-company-prop">
                                        <div class="star-company">
                                            <div class="star">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="review-prop">
                                            <div class="quantity">
                                                <p>Отзывов: <span>114</span></p>
                                            </div>
                                            <div class="percent">
                                                <p>98<span>% положительных</span></p>
                                            </div>
                                        </div>
                                    </div>
                                <div class="price-company confirmed-price">
                                    <p>2077 <span>€</span></p>
                                </div>
                                <div class="confirm_offer">
                                    <p>Предложение принято, запрос отправлен</p>
                                    <div class="right-col">
                                        <div class="suggestion confirmed-proposal">
                                            <p>Предложение подтверждено</p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>


                        </div>
                    </div>


                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/lk_list_order/lk_list_order.js"></script>
<script type="text/javascript" src="/js/lk_list_order/popup.js"></script>
<?php include("template/popupMap.php") ?>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

<?php include("template/header-lk.php") ?>
<div class="section-lk-top">
    <div class="container">
        <div class="top">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12 p-l p-r">
                    <div class="name-person">
                        <div class="all">
                            <h4>Дюсельдорф Фонмюллеркренсербернгмингер</h4>
                            <p>40219 Бад-Мюнстер-на-Штайн-Эбернбурге </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <div class="but-btn but-lk">
                        <a href="/order.php">Оставить заявку</a>
                    </div>
                </div>
                <div class="all-demens">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 not-pad">
                        <div class="btn-black active btn-dimensions my_application">
                            <a href="/lk_client_list.php">Мои заявки</a>
                        </div>
                        <a href="#" class="red-circle"></a>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 not-pad">
                        <div class="btn-black btn-dimensions">
                            <a href="#">Архив заявок</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-news">
            <div class="col-sm-12">
                <div class="block-sort">
                    <div class="name-block">
                        <p>
                            Сортировка
                        </p>
                    </div>
                    <div class="descript">
                        <select class="change_order" name="sort">
                            <option>от новых к старым</option>
                            <option>от старых к новым</option>
                            <option>подтвержденные</option>
                        </select>
                    </div>
                </div>
                <div class="section-my-applications">
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/order-relocation/tabs3/2.jpg" alt="">
                        </div>
                    </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="/lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion have-proposal">
                                <p>У Вас есть новое предложение</p>
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/lk_list/block1/img2.png" alt="">
                        </div>
                        </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion confirmed-proposal">
                                <p>Предложение подтверждено</p>
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/lk_list/block1/img3.png" alt="">
                        </div>
                        </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion">
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/lk_list/block1/img1.png" alt="">

                        </div>
                        </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion have-proposal">
                                <p>У Вас есть новое предложение</p>
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/lk_list/block1/img4.png" alt="">
                        </div>
                        </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion confirmed-proposal">
                                <p>Предложение подтверждено</p>
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                    <div class="application">
                        <a href="/lk_client_order.php">
                        <div class="left-col">
                            <img src="img/lk_list/block1/img2.png" alt="">
                        </div>
                        </a>
                        <div class="center-col">
                            <div class="bid-name">
                                <div class="l-col">
                                    <a href="lk_client_order.php">Переезд</a>
                                </div>
                                <div class="r-col">
                                    <p>ID <span>3584102</span></p>
                                </div>
                            </div>
                            <div class="descript">
                                <div class="list">
                                    <div class="name">
                                        <p>Откуда:</p>
                                    </div>
                                    <div class="text">
                                        <p>50674 Кельн, Германия</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Куда:</p>
                                    </div>
                                    <div class="text">
                                        <p>60329 Франкфурт-на-Майне</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Расстояние:</p>
                                    </div>
                                    <div class="text">
                                        <p>Около 500 км (по прямой)</p>
                                    </div>
                                </div>
                                <div class="list">
                                    <div class="name">
                                        <p>Дата выполнения заказа:</p>
                                    </div>
                                    <div class="text">
                                        <p>дд.мм.гггг</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="right-col">
                            <div class="suggestion">
                                <a href="/lk_client_order.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="line_shadow"></div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="pagination pagination-list">
                    <a class="arrow-left" href="#"></a>
                    <a class="active number" href="#">1</a>
                    <a class="number" href="#">2</a>
                    <a class="number" href="#">3</a>
                    <a class="more" href="#">...</a>
                    <a class="number" href="#">7</a>
                    <a class="number" href="#">8</a>
                    <a class="arrow-right" href="#"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

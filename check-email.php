<?php include("template/header-main.php") ?>

<div class="section-application check-email">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="application">
                    <h2>Проверьте свою электронную почту</h2>
                    <div class="center-info">
                        <div class="pict">
                            <img src="img/order-relocation/tabs3/prinato.svg" alt="">
                        </div>
                        <div class="decript desc-email">
                            <p>Рады видеть Вас в кругу своих клиентов.</p>
                            <p>На адрес Вашей электронной почты, указанной при регистрации, отправлено письмо со ссылкой активации учетной записи. Пожалуйста, активируйте свою учетную запись, нажав на ссылку в этом электронном письме.</p>
                        </div>
                        <div class="return_to_site">
                            <div class="arrow">
                                <a href="/">
                                    <img src="img/login/arrow.svg" alt="">
                                </a>
                            </div>
                            <div class="return">
                                <a href="/">Вернуться на сайт</a>
                            </div>
                        </div>
                        <h5>
                            Желаем успехов! <br>
                            С уважением, команда Effektiv Service!
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("template/footer.php") ?>

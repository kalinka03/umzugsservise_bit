<?php include("template/header-main.php") ?>

<div class="section-partner-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Выгодное сотрудничество</h2>
                    <ul>
                        <li>Если вы транспортная компания</li>
                        <li>Если вы клининговая компания</li>
                        <li>Если у вас есть большой склад</li>
                        <li>Если вы хотите организовать службу доставки</li>
                        <li>Если вы ищете новые заказы</li>
                        <li>Если у вас есть вопросы или предложения по сотрудничеству</li>
                    </ul>
                </div>
                <div class="mob_partners" data-aos="fade-right"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200">
                    <div class="left-pict" data-aos="fade-right"
                         data-aos-offset="100" data-aos-duration="800"
                         data-aos-easing="ease-in-sine" data-aos-delay="200">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="contact">
                <div class="contact-partner b-all">
                    <a href="#js-qustion">Свяжитесь с нами</a>
                </div>
            </div>
            <div class="mob_man" data-aos="fade-right"
                 data-aos-offset="100" data-aos-duration="800"
                 data-aos-easing="ease-in-sine" data-aos-delay="200">
                <img src="img/partners/block1/man2.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="section-advantages">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Преимущества для партнеров</h2>
                </div>
            </div>
            <div class="list-advantages">
                <div class="col-sm-4" data-aos="fade-left" data-aos-offset="100" data-aos-delay="100"
                     data-aos-once="true">
                    <div class="item">
                        <div class="col-icon">
                            <div class="icon client">
                            </div>
                            <div class="info">
                                <h4>Легкий доступ к новым клиентам</h4>
                            </div>
                        </div>
                        <div class="text">
                            <p>Удобный гибкий поиск новых заказов помогает оперативно отслеживать интересующие вас
                                запросы.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" data-aos="fade-left" data-aos-offset="300" data-aos-delay="300"
                     data-aos-once="true">
                    <div class="item">
                        <div class="col-icon">
                            <div class="icon business">
                            </div>
                            <div class="info">
                                <h4>Удобство ведения бизнеса</h4>
                            </div>
                        </div>
                        <div class="text">
                            <p>Все заказы в одном месте. Вы можете выступать в качестве исполнителя и клиента, полностью
                                или частично передавать заказы на выполнение другим компаниям.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" data-aos="fade-left" data-aos-offset="400" data-aos-delay="600"
                     data-aos-once="true">
                    <div class="item">
                        <div class="col-icon">
                            <div class="icon constraints">
                            </div>
                            <div class="info">
                                <h4>Отсутствие ограничений</h4>
                            </div>
                        </div>
                        <div class="text">
                            <p>Безлимит по контактам с отсутствием комиссий и скрытых платежей. Вы сами регулируете
                                количество обрабатываемых заказов.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="advantages-for-partners">
                    <p data-aos="fade-down" data-aos-offset="200" data-aos-delay="600" data-aos-once="true">Lorem ipsum
                        dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-become-partner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Как стать партнером</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4" data-aos="zoom-in-right" data-aos-delay="=100" data-aos-once="true">
                <div class="step">
                    <div class="foto">
                        <img src="img/partners/block3/foto1.png" alt="">
                    </div>
                    <div class="name">
                        <div class="left-block">
                            <p>1</p>
                        </div>
                        <div class="rigth-block">
                            <p>Заполнить регистрационную анкету</p>
                        </div>
                    </div>
                </div>
                <div class="arrrow-step">
                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                </div>
            </div>
            <div class="col-sm-4" data-aos="zoom-in-right" data-aos-delay="500" data-aos-once="true">
                <div class="step">
                    <div class="foto">
                        <img src="img/partners/block3/foto2.png" alt="">
                    </div>
                    <div class="name">
                        <div class="left-block">
                            <p>2</p>
                        </div>
                        <div class="rigth-block">
                            <p>Пройти проверку, пробный период</p>
                        </div>
                    </div>
                </div>
                <div class="arrrow-step">
                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                </div>
            </div>
            <div class="col-sm-4" data-aos="zoom-in-right" data-aos-delay="800" data-aos-once="true">
                <div class="step">
                    <div class="foto">
                        <img src="img/partners/block3/foto3.png" alt="">
                    </div>
                    <div class="name">
                        <div class="left-block">
                            <p>3</p>
                        </div>
                        <div class="rigth-block">
                            <p>Оплата абонемента, полный доступ к базе заказов</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="plan-business">
            <div class="frame">
                <div class="wrapper-table">
                    <div class="wrapper-table-container" data-aos="zoom-in-right" data-aos-delay="100"
                         data-aos-once="true">
                        <div class="column-l"></div>
                        <div class="column-s">Business Basic
                        </div>
                        <div class="column-r">Business Pro
                        </div>
                    </div>
                    <div class="wrapper-table-row">
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="100"
                             data-aos-once="true">
                            <div class="table-col-l">Доступ к поиску и обработке заказов</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s"> Доступно</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Доступно
                            </div>
                        </div>
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="200"
                             data-aos-once="true">
                            <div class="table-col-l">Приоритет отображения предложений клиентам</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s"> Низкий</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Высокий
                            </div>
                        </div>
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="300"
                             data-aos-once="true">
                            <div class="table-col-l">Возможность запроса стоимости от клиента</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s"> Нет</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Доступно
                            </div>
                        </div>
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="400"
                             data-aos-once="true">
                            <div class="table-col-l">Объем предложения</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s">До 60 м2</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Без ограничений
                            </div>
                        </div>
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="500"
                             data-aos-once="true">
                            <div class="table-col-l">Профиль компании</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s"> Доступно</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Доступно
                            </div>
                        </div>
                        <div class="wrapper-table-col" data-aos="zoom-in-right" data-aos-delay="600"
                             data-aos-once="true">
                            <div class="table-col-l">Отзывы клиентов</div>
                            <div class="table-col-bb"> Business Basic</div>
                            <div class="table-col-s"> Доступно</div>
                            <div class="table-col-br"> Business Pro</div>
                            <div class="table-col-r">
                                Доступно
                            </div>
                        </div>


                    </div>
                </div>
                <div class="basic_plan contact-partner b-all" data-aos="zoom-in-right" data-aos-delay="100"
                     data-aos-once="true">
                    <a href="#">Начните прямо сейчас</a>
                </div>
                <p>Бесплатный пробный период 3 дня по 3 предложения</p>
            </div>
        </div>
    </div>
</div>
<div class="section-answers">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>5 наиболее распространенных вопросов</h2>
                </div>
                <div class="tabs_answers">
                    <div class="all">
                        <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                            <div class="arrow">
                                <img src="img/partners/block3/6_chekmark.svg" alt="">
                            </div>
                        </div>
                        <div class="answer_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus pronin sapien nunc accuan eget.</p>
                        </div>

                    </div>

                    <div class="all">
                        <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                            <div class="arrow">
                                <img src="img/partners/block3/6_chekmark.svg" alt="">
                            </div>
                        </div>
                        <div class="answer_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus pronin sapien nunc accuan eget.</p>
                        </div>

                    </div>

                    <div class="all">
                        <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                            <div class="arrow">
                                <img src="img/partners/block3/6_chekmark.svg" alt="">
                            </div>
                        </div>
                        <div class="answer_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus pronin sapien nunc accuan eget.</p>
                        </div>

                    </div>

                    <div class="all">
                        <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                            <div class="arrow">
                                <img src="img/partners/block3/6_chekmark.svg" alt="">
                            </div>
                        </div>
                        <div class="answer_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus pronin sapien nunc accuan eget.</p>
                        </div>

                    </div>

                    <div class="all">
                        <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                            <div class="arrow">
                                <img src="img/partners/block3/6_chekmark.svg" alt="">
                            </div>
                        </div>
                        <div class="answer_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus pronin sapien nunc accuan eget.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="more-info-partners">

                        <a href="#">Больше информации в разделе FAQ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/partners.js"></script>
    <?php include("template/popUps.php") ?>
    <?php include("template/footer.php") ?>

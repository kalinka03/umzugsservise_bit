<?php include("template/header-main.php") ?>

<div class="section-application check-email">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="application">
                    <h2>Проверьте свою электронную почту</h2>
                    <div class="center-info">
                        <div class="pict">
                            <img src="img/order-relocation/tabs3/prinato.svg" alt="">
                        </div>
                        <div class="decript">
                            <p>На Ваш почтовый ящик отправлено письмо с дальнейшими инструкциями</p>
                        </div>
                        <h5>
                            Желаем успехов! <br>
                            С уважением, команда Effektiv Service!
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("template/footer.php") ?>

<?php include("template/header-main.php") ?>

<div class="section-application rec-password">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="application">
                    <h2>Восстановление пароля</h2>
                    <div class="center-info">
                    <div class="decript">
                        <p>Для изменения пароля, пожалуйста, укажите адрес, использованный Вами при регистрации</p>
                    </div>
                    <form class="form-all form-group">
                        <input type="hidden"  name="Восстановление пароля">
                        <div>
                            <input class="popup-call__email" type="text" name="email" placeholder="Email">
                        </div>
                        <div class="but-btn login-btn bot-btn">
                            <button>Отправить</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/restore_password/validateController.js"></script>
<?php include("template/footer.php") ?>

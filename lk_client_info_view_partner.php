<?php include("template/header-lk.php") ?>
<div class="section-lk-top">
    <div class="container">
        <div class="content-news partner_company_content">
            <div class="block-partner_information_top">
                <div class="foto-partner">
                    <img src="img/lk_client_info_partner/page1/logo.png" alt="">
                </div>
                <div class="center-col">
                    <div class="company_name">
                        <div class="name">
                            <p>Название компании на случай если очень длинное название компании</p>
                        </div>
                        <div class="number">
                            <p>ID <span>2510603</span></p>
                        </div>
                    </div>
                    <div class="adress">
                        <p>13409 Bad Münster am Stein-Ebernburg</p>
                    </div>
                    <div class="star-company">
                        <div class="star">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                            <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="right-col">
                    <div class="reviews">
                        <a href="#reviews_company">Отзывов <span>114</span></a>
                    </div>
                    <div class="percent">
                        <p>98 <span>% положительных </span></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="document-partner">
                    <div class="title">
                        <h2>
                            Квалификация компании
                        </h2>
                    </div>



                    <div class="doc_confirmation">
                        <p>Документы подтверждены</p>
                    </div>
                </div>
                <div class="doc-qualification">
                    <div class="item">
                        <p>Möbelmontageservice</p>
                    </div>
                    <div class="item">
                        <p>Wasserinstallation / Nachweis</p>
                    </div>
                    <div class="item">
                        <p>Küchenmontageservice Schreiner/Tischler</p>
                    </div>
                    <div class="item">
                        <p>Einlagerungsservice</p>
                    </div>
                    <div class="item">
                        <p>Nachweis</p>
                    </div>
                    <div class="item">
                        <p>EDF – Service</p>
                    </div>
                    <div class="item">
                        <p>Elektroservice / Nachweis</p>
                    </div>
                </div>

                <div class="all_title_partner">
                    <h2>
                        Описание компании
                    </h2>
                </div>
                <div class="descript-partner">
                    <p>С правильной движущейся компанией в мгновение ока в новом доме!</p>
                    <p>Передвижные компании вытесняют рынок. Но как вы находите именно компанию среди всех провайдеров,
                        которые оживляют ваш переход к жизни довольно спокойно и плавно? А что же дешево?</p>
                    <p>Просто введите в Интернете по адресу www.effektiv-umzuege.de самую важную информацию о месте
                        отправления и месте назначения, а также о других желаемых услугах (например, демонтаж кухни или
                        строительство мебели). Впоследствии ваш запрос будет отправлен нам. Мы сделаем для вас
                        индивидуальное предложение без каких-либо обязательств.</p>

                    <p>Наши услуги для вас.</p>
                    <ul>
                        <li><span>Ближний и дальний переезды</span></li>
                        <li><span>Частные и компании изъятия</span></li>
                        <li><span>Съемки на старших участках</span></li>
                        <li><span>Мебельный транспорт</span></li>
                        <li><span>Обслуживание сборки мебели и кухни</span></li>
                        <li><span>Entrümpelungsservice</span></li>
                        <li><span>Службы хранения</span></li>
                        <li><span>Небольшие транспорты</span></li>
                    </ul>
                    <ol>
                        <li><span>Ближний и дальний переезды</span></li>
                        <li><span>Частные и компании изъятия</span></li>
                        <li><span>Съемки на старших участках</span></li>
                        <li><span>Мебельный транспорт</span></li>
                    </ol>
                    <p>В случае заказа мы уже заверяем вас в тщательном и профессиональном выполнении всей работы.
                        Взгляните на наши отзывы здесь:</p>
                    <a href="#">http://www.kennstdueinen.de/umzug-delmenhorst-eugen-teschner-umzuege-transporte-d39798.html</a>
                    <p>**** СЕЙЧАС ТАКЖЕ НОВЫЙ 24h через WHATSAPP достижимый 0170-8619458 !! ***</p>
                </div>
                <div class="history_company">
                    <div class="item">
                        <div class="l-col">
                            <p>Форма регистрации:</p>
                        </div>
                        <div class="r-col">
                            <p>Natürliche Person/Einzelunternehmen</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="l-col">
                            <p>Количество сотрудников:</p>
                        </div>
                        <div class="r-col">
                            <p>4-9</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="l-col">
                            <p>Год основания:</p>
                        </div>
                        <div class="r-col">
                            <p>2000</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="l-col">
                            <p>Страховые обязательства:</p>
                        </div>
                        <div class="r-col">
                            <p>да</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="l-col">
                            <p>Управляющий/владелец:</p>
                        </div>
                        <div class="r-col">
                            <p>Фридрих Оттовордемгентшенфельд</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="l-col">
                            <p>Часы работы:</p>
                        </div>
                        <div class="r-col">
                            <p>Пн-Пт: 9<sup>00</sup>-18<sup>00</sup> <br> Сб: 9<sup>00</sup>-16<sup>00</sup></p>
                        </div>
                    </div>
                </div>
                <div class="slider-foto-parnter">
                    <div class="slider-partner-company owl-carousel">
                        <div class="slider-item">
                            <a href="#js_popup_slider_foto">
                            <div class="foto-partners">
                                <img src="img/lk_client_info_partner/page1/1.png" alt="">
                            </div>
                            </a>
                        </div>
                        <div class="slider-item">
                            <a href="#js_popup_slider_foto">
                            <div class="foto-partners">
                                <img src="img/lk_client_info_partner/page1/2.png" alt="">
                            </div>
                            </a>
                        </div>
                        <div class="slider-item">
                            <a href="#js_popup_slider_foto">
                            <div class="foto-partners">
                                <img src="img/lk_client_info_partner/page1/3.png" alt="">
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="all_title_partner">
                    <h2>
                        Контактные данные
                    </h2>
                </div>
                <div class="block-map-partners">
                    <div class="contacs">
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/user.svg" alt="">
                            </div>
                            <div class="info">
                                <div class="contact_all">
                                    <p>Фридрих Оттовордемгентшенфельд</p>
                                    <p class="small_contact-name">контактное лицо</p>
                                </div>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/telephone.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="tel:+491231234567">+49 123 1234567</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/smartphone.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="tel:+491231234567">+49 123 1234567</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/fax.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="tel:+491231234567">+49 123 1234567</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/grid-world.svg" alt="">
                            </div>
                            <div class="info">
                                <a target="_blank " href="http://companysite.com">companysite.com</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/mail.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="mailto:mail@companysite.com">mail@companysite.com</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/lk_client_info_partner/page1/map-placeholder.svg" alt="">
                            </div>
                            <div class="info">
                                <p>Потсдамское шоссе, 78 <span></span>13409 Берлин</p>
                            </div>
                        </div>
                    </div>
                    <div id="map_adress_partner_company"></div>
                </div>

                <div class="all_title_partner reviews-Partn">
                    <div id="reviews_company" class="reviews-company_partner">
                        <h2>
                            Отзывы о компании
                        </h2>
                    </div>
                    <div class="all-reviews-company">
                        <div class="rew">
                            <div class="star">
                                <img src="img/main/block7/7_star.svg" alt="">
                                <img src="img/main/block7/7_star.svg" alt="">
                                <img src="img/main/block7/7_star.svg" alt="">
                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                            </div>
                            <p>Отзывов <span>114</span></p>
                        </div>
                        <div class="percent">
                            <p>98<span>% положительных</span></p>
                        </div>
                    </div>
                </div>
                <div class="some-review-company">
                    <div class="l-col">
                        <div class="block-info">
                            <div class="name">
                                <h5>Имя Фамилия</h5>
                                <p>50674 Кельн, Германия</p>
                                <p>Добавлено <span>дд.мм.гггг.</span></p>
                            </div>
                            <div class="more-info-company">
                                <p>Цена:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество выполнения заказа:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество работы сотрудников:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество обслуживания компании:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="r-col">
                        <div class="text-review-company">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci,
                                sed rhoncus sapien nunc eget.</p>
                        </div>
                        <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                        <a class="close_reviews_company_partner" href="#">Скрыть</a>
                    </div>
                </div>
                <div class="some-review-company">
                    <div class="l-col">
                        <div class="block-info">
                            <div class="name">
                                <h5>Имя Фамилия</h5>
                                <p>50674 Кельн, Германия</p>
                                <p>Добавлено <span>дд.мм.гггг</span></p>
                            </div>
                            <div class="more-info-company">
                                <p>Цена:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество выполнения заказа:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество работы сотрудников:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество обслуживания компании:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <div class="block-recommend">
                                    <p>Pекомендую</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="r-col">
                        <div class="text-review-company">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                                sed rhoncus sapien nunc eget.</p>
                        </div>
                        <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                        <a class="close_reviews_company_partner" href="#">Скрыть</a>
                    </div>
                </div>
                <div class="some-review-company">
                    <div class="l-col">
                        <div class="block-info">
                            <div class="name">
                                <h5>Имя Фамилия</h5>
                                <p>50674 Кельн, Германия</p>
                                <p>Добавлено <span>дд.мм.гггг</span></p>
                            </div>
                            <div class="more-info-company">
                                <p>Цена:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество выполнения заказа:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество работы сотрудников:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <p>Качество обслуживания компании:</p>
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="r-col">
                        <div class="text-review-company">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                orci,
                                sed rhoncus sapien nunc eget.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                orci,
                                sed rhoncus sapien nunc eget.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                orci,
                                sed rhoncus sapien nunc eget.</p>
                        </div>
                        <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                        <a class="close_reviews_company_partner" href="#">Скрыть</a>
                    </div>
                </div>
                <div class="btn-bottom b-all but_rew_company">
                    <a href="/lk_client_comment_view_partner.php">Читать все отзывы</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/lk_client_info_view_partner/lk_client_info_view_partner.js"></script>
<script type="text/javascript" src="/js/openCommit.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

<?php include("template/header-lk.php") ?>
<div class="section-lk-top">
    <div class="container">
        <div class="col-sm-12">
            <div class="come_back">
                <a href="/lk_client_comment_view_partner.php">Назад</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="name_block_comment">
            <h2>Оставить отзыв</h2>
            <div class="block_fulfill_order">
                <div class="l-col">
                    <p>На выполнение заказа</p>
                </div>
                <div class="r-col">
                    <p>Переезд <span>ID 2510603</span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-news">
            <div class="block-partner_information_top all_comment_partner">
                <div class="foto-partner">
                    <img src="img/lk_client_info_partner/page1/logo.png" alt="">
                </div>
                <div class="center-col">
                    <div class="company_name">
                        <div class="name">
                            <p>Название компании на случай если очень длинное название компании</p>
                        </div>
                        <div class="number">
                            <p>ID <span>2510603</span></p>
                        </div>
                    </div>
                    <div class="adress">
                        <p>13409 Bad Münster am Stein-Ebernburg</p>
                    </div>
                    <div class="star-company">
                        <div class="star">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img src="img/main/block7/7_star.svg" alt="">
                            <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                            <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">
                <form class="give_feedback_partner">
                    <div class="overall order-comment">
                        <div class="overall-rating">
                            <div class="title">
                                <h2>
                                    Оценка
                                </h2>
                            </div>
                            <p>Оцените качество выполнения заказа по этим параметрам:</p>
                            <div class="overall-price">
                                <div class="all_comment">
                                    <label for="name">Цена:</span></label>
                                    <div class="check-star assessment">
                                        <div class="star">
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                        </div>
                                        <div class="characteristic">
                                            <input type="hidden" name="stars_check" value="">
                                            <span class="hint_star"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="quality_order">
                                <div class="all_comment">
                                    <label for="name">Качество выполнения заказа:</span></label>
                                    <div class="check-star assessment">
                                        <div class="star">
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                        </div>
                                        <div class="characteristic">
                                            <input type="hidden" name="stars_check" value="">
                                            <span class="hint_star"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="quality_work_employees">
                                <div class="all_comment">
                                    <label for="name">Качество работы сотрудников:</span></label>
                                    <div class="check-star assessment">
                                        <div class="star">
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                        </div>
                                        <div class="characteristic">
                                            <input type="hidden" name="stars_check" value="">
                                            <span class="hint_star"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="quality_service">
                                <div class="all_comment">
                                    <label for="name">Качество обслуживания компании:</span></label>
                                    <div class="check-star assessment">
                                        <div class="star">
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                            <span class="checkmark-star"></span>
                                        </div>
                                        <div class="characteristic">
                                            <input type="hidden" name="stars_check" value="">
                                            <span class="hint_star"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="overall-block-company overall_order_block_commit">
                            <div class="title">
                                <h2>
                                    Комментарий:
                                </h2>
                            </div>
                            <div class="commit">
                                <div class="commit_text">
                                    <textarea name="commit" cols="30" rows="10" placeholder="Комментарий"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="recommend-company">
                        <div class="title">
                            <h2>
                                Рекомендуете ли вы эту компанию другим клиентам?
                            </h2>
                        </div>
                        <div class="item">
                            <label>
                                <input class="radio" type="radio" name="radio_test" value="Да">
                                <span class="radio-custom"></span>
                                <span class="label">Да</span>
                            </label>
                        </div>
                        <div class="item">
                            <label>
                                <input class="radio" type="radio" name="radio_test" value="Нет">
                                <span class="radio-custom"></span>
                                <span class="label">Нет</span>
                            </label>
                        </div>
                        <div class="item">
                            <label>
                                <input id="other" class="radio" type="radio" name="recomed" value="Затрудняюсь ответить">
                                <span class="radio-custom"></span>
                                <span class="label">Затрудняюсь ответить</span>
                            </label>
                        </div>
                    </div>
                    <div class="but-btn give_feedback">
                        <button>Отправить</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/lk_client_order_comment/lk_client_order_comment.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

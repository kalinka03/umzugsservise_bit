<?php include("template/header-lk.php") ?>

<div class="section-lk-top">
    <div class="section-ordering-services">
        <div class="container">
            <div class="row">
                <div class="order-relocation">
                    <h3>Хранение вещей</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myTab" class="section-choice">
    <div class="section-steps-relocation">
        <div class="container">
            <div class="row">
                <div class="steps">
                    <div class="step-menu nav-tabs">
                        <button disabled class="sk_tab filter active">
                            Расположение
                        </button>
                        <button disabled class="sk_tab filter">
                           Список вещей
                        </button>
                        <button disabled class="sk_tab filter">
                            Контактные данные
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form id="#form_sklad">
                    <div class="step-content step-content-sklad">
                        <div class="content-choice sk_content active">
                            <div class="geography-block">
                                <div class="order-lk form-group form-all">
                                    <p>Укажите адрес размещения Ваших вещей и все сопутствующие параметры и мы сможем подобрать для Вас лучшие предложения</p>
                                    <h4>Расположение</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="adress-lk">
                                            <div class="v-d-index index">
                                                <input  class="index pageRequired change_index_sk" type="tel" name="index"
                                                       placeholder="Индекс"
                                                       maxlength="5">
                                                <span id="valid"></span>
                                            </div>
                                            <div class="v-d-city">
                                                <input class="city pageRequired name_city_for_index" type="text" name="city"
                                                       placeholder="Город" value="">
                                                <span id="valid"></span>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <input class="adress-form pageRequired" type="text" name="adress"
                                                   placeholder="Адрес">
                                            <span id="valid"></span>
                                        </div>
                                    </div>
                                    <h4>Период хранения</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Начало периода хранения:</label>
                                                <input class="sk_calendar popup-calendar datepicker_from pageRequired"
                                                       name="date_from"
                                                       placeholder="Начало периода хранения:" type="text">
                                            </div>
                                            <div class="r-all sklad_calendar">
                                                <label class="all-label">Конец периода хранения:</label>
                                                <input class="sk_calendar popup-calendar datepicker_for pageRequired"
                                                       name="date_for"
                                                       placeholder="Конец периода хранения:" type="text">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <label class="all-label"> Комментарий к услуге:</label>
                                            <textarea class="sklad_commit pageRequired" rows="3" name="commit"
                                                      placeholder="Комментарий"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="but-btn center-but btnNext btnNext_sklad">
                                    <button>Далее</button>
                                </div>
                            </div>
                        </div>
                        <div class="content-choice  sk_two_content">
                            <div class="list-things-block">
                                <div class="form-group form-all choice-block-things">
                                    <h4>Составьте инвентаризацию своих вещей</h4>
                                    <div class="step-things-tree-block"></div>
                                    <p class="js_things_info"> Составление списка вещей способствует более точной оценке объема помещения, необходимого для хранения Ваших вещей.</p>
                                    <p class="js_things_info">
                                        Выберите любой удобный способ предоставления списка для оценки и предоставления выгодного предложения.</p>
                                    <div class="downloading-files">
                                        <div class="list">
                                            <div class="item create_list_js">
                                                <div class="left-col document">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Создать список онлайн</h3>
                                                            <p>Найдите и добавьте вещи, требущие хранения</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col">
                                                        <div id="1" class="list-things-bt create_list_js">
                                                            <button>Создать список</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item open_foto">
                                                <div class="left-col foto">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить фото</h3>
                                                            <p>Объем фото не должен превышать 5 Mb</p>
                                                        </div>
                                                    </div>
                                                    <div class="right-col">
                                                        <div id="2" class="list-things-bt open_foto">
                                                            <button>Прикрепить фото</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-foto hover-downloading-foto-sk">
                                                <div class="foto-block">
                                                    <div class="new-fot add-plus">
                                                        <label for="uploadbtn" class="uploadButton"></label>
                                                        <input style="opacity: 0; z-index: -1;" type="file"
                                                               name="upload" id="uploadbtn">
                                                    </div>
                                                    <div class="imgg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item open_download_file">
                                                <div class="left-col things">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить готовый список вещей</h3>
                                                            <p>Можно прикрепить файлы формата pdf/doc/xls</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col">
                                                        <div id="3" class="list-things-bt open_download_file">
                                                            <div class="file-upload">
                                                                <label>
                                                                    <input type="file" name="file" val="">
                                                                    <span>Прикрепить документ</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-file hover-downloading-foto-sk">
                                                <div class="file-block">
                                                    <p>Загружен файл</p>
                                                    <div class="fileDownload">
                                                        <input id="filename" class="filename"> </input>
                                                        <div class="del-file"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn btnPrevious">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  btnNext_sklad submission-sk">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="create-things">
                                    <p>Составление списка вещей способствует оптимизации переезда.</p>
                                    <p> Найдите и добавьте вещи для каждого имеющегося у Вас помещения.</p>
                                    <div class="catalog-things hover-downloading-foto-sk">
                                        <div class="choise-catalog-things sk_choice_things">
                                            <div class="tab-content-things">
                                                <div class="content-choice-things">
                                                    <div class="addThings untitled">
                                                    </div>
                                                    <div class="all-quantity-things">
                                                        <input type="hidden" id="area_sk_things">
                                                        <h2>Всего <span class="quantity-things"></span> <span>м<sup>3</sup></span></h2>
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button disabled>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things sk_found">
                                                        <input id="things_filter_show" type="text" placeholder="Поиск">
                                                        <div class="things">
                                                            <div class="item OneThing" value="Книжная полка" data-id="1"
                                                                 src="img/order-relocation/tabs3/furniture/1.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Сервант" data-id="2"
                                                                 src="img/order-relocation/tabs3/furniture/6Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item OneThing" value="Телевизор" data-id="3"
                                                                 src="img/order-relocation/tabs3/furniture/11TV.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item OneThing" value="Вазон" data-id="4"
                                                                 src="img/order-relocation/tabs3/furniture/16Plant.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стол" data-id="5"
                                                                 src="img/order-relocation/tabs3/furniture/2Table.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item OneThing" value="Буфет" data-id="6"
                                                                 src="img/order-relocation/tabs3/furniture/7Buffet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стерео" data-id="7"
                                                                 src="img/order-relocation/tabs3/furniture/12Stereo.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item OneThing" value="Коробка" data-id="8"
                                                                 src="img/order-relocation/tabs3/furniture/17Box.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item OneThing" value="Диван" data-id="9"
                                                                 src="img/order-relocation/tabs3/furniture/3Sofa.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item OneThing" value="Тумба" data-id="10"
                                                                 src="img/order-relocation/tabs3/furniture/8Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item OneThing" value="Картина" data-id="11"
                                                                 src="img/order-relocation/tabs3/furniture/13Picture.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item OneThing" value="Кресло" data-id="12"
                                                                 src="img/order-relocation/tabs3/furniture/4Armchair.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item OneThing" value="Шкаф" data-id="13"
                                                                 src="img/order-relocation/tabs3/furniture/9Cupboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item OneThing" value="Люстра" data-id="14"
                                                                 src="img/order-relocation/tabs3/furniture/14Chandelier.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item OneThing" value="Стул" data-id="15"
                                                                 src="img/order-relocation/tabs3/furniture/5Sideboard.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item OneThing" value="Ковер" data-id="16"
                                                                 src="img/order-relocation/tabs3/furniture/10Carpet.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item OneThing" value="Торшер" data-id="17"
                                                                 src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add" data-id="18">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn catalogEngine">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  submission-btn btnNext_sklad">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="content-choice sk_three_content">
                                <div class="center-block">
                                    <div class="geography-block">
                                        <div class="order-lk form-group form-all">
                                            <p>Укажите, пожалуйста, Ваши контактные данные. В ближайшее время на адрес
                                                Вашей электронной почты будет отправлено письмо со ссылкой, по которой
                                                Вы сможете просмотреть полученные предложения.</p>
                                            <h4>Укажите Ваши контактные данные</h4>
                                            <div class="recourse-lk contact-things">
                                                <div class="recourse-name">
                                                    <div class="item">
                                                        <label>
                                                            <input class="radio" type="radio" name="radio-test"
                                                                   value="Господин" checked>
                                                            <span class="radio-custom"></span>
                                                            <span class="label">Господин</span>
                                                        </label>
                                                    </div>
                                                    <div class="item">
                                                        <label>
                                                            <input class="radio" type="radio" name="radio-test"
                                                                   value="Госпожа">
                                                            <span class="radio-custom"></span>
                                                            <span class="label">Госпожа</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-profile lk-rel">
                                                <div class="cont-things-form">
                                                    <div class="item">
                                                        <input type="text" name="name" placeholder="Ваше имя">
                                                    </div>
                                                    <div class="item">
                                                        <input type="text" name="surname" placeholder="Фамилия">
                                                    </div>
                                                    <div class="item">
                                                        <input type="email" name="email" placeholder="Email">
                                                    </div>
                                                    <div class="item">
                                                        <input class="form-control" placeholder="Телефон" name="phone"
                                                               id="Piramida_phone" type="tel" maxlength="20"/>
                                                        <div class="help-block error" id="Piramida_phone_em_"
                                                             style="display:none"></div>
                                                        <input type="checkbox" id="phone_mask" placeholder="Телефон"
                                                               checked>
                                                        <label id="descr" for="phone_mask" class="phone-mask">Использовать
                                                            маску</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="arrow-steps">
                                            <div class="but-btn btnPrevious">
                                                <button>Назад</button>
                                            </div>
                                            <div class="but-btn btnSubmit">
                                                <button>Отправить заявку</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="hover_elements">
Проверка данной страницы
</div>


<script type="text/javascript" src="/js/lk_sklad/mainControllerSk.js"></script>
<script type="text/javascript" src="/js/lk_order/tabsController.js"></script>
<script type="text/javascript" src="/js/lk_order/thirdTabsEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/tabListEngine.js"></script>
<script type="text/javascript" src="/js/lk_sklad/validateController.js"></script>
<script type="text/javascript" src="/js/lk_sklad/AreaSk.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/uploadPhoto.js"></script>
<!--<script type="text/javascript" src="/js/lk_other/templateEngine/templateEngineEvents.js"></script>-->
<script type="text/javascript" src="/js/lk_order/showListThings.js"></script>
<script type="text/javascript" src="/js/lk_order/listPhotoTab2.js"></script>
<script type="text/javascript" src="/js/lk_other/filterThings/filterOneThings.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateOtherEngineEvents.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateEngineEvents.js"></script>
<script type="text/javascript" src="/js/lk_other/templateEngine/templateEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/PhotoGalleryEngine.js"></script>
<script type="text/javascript" src="/js/lk_order/checkingDimensions.js"></script>
<!--<script type="text/javascript" src="/js/lk_order/areaThings.js"></script>-->
<script type="text/javascript" src="/js/lk_other/filterThings/filterThings.js"></script>
<script type="text/javascript" src="/js/lk_order/eventNewThing.js"></script>
<script type="text/javascript" src="/js/lk_order/arrayThings.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLTemplates.js"></script>
<script type="text/javascript" src="/js/system/indexCityStorange.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLThings.js"></script>
<script type="text/javascript" src="/js/lk_order/HTML/HTMLOtherThings.js"></script>
<script type="text/javascript" src="/js/lk_sklad/thirdTabSkEngine.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/AddThingsOneArea.js"></script>
<script type="text/javascript" src="/js/lk_other/addFoto/AddThingsOtherArea.js"></script>
<script type="text/javascript" src="/js/lk_other/validateNumber.js"></script>
<script type="text/javascript" src="/js/lk_sklad/nexTab.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

<?php include("template/header-lk.php") ?>

<div class="section-application">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="application">
                    <h2>Ваша заявка принята</h2>
                    <div class="center-info">
                        <div class="pict">
                            <img src="img/order-relocation/tabs3/prinato.svg" alt="">
                        </div>
                        <div class="decript">
                            <p>Рады видеть Вас в кругу своих клиентов.</p>
                            <p>В ближайшее время на адрес Вашей электронной почты будет отправлено письмо со ссылкой, по
                                которой Вы сможете просмотреть полученные предложения.</p>
                            <p>Для более комфортного пользования услугами сайта перейдите в <a
                                        href="/lk_client.php"> личный кабинет</a></p>
                        </div>
                        <h5>
                            Желаем успехов! <br>
                            С уважением, команда Effektiv Service!
                        </h5>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("template/footer.php") ?>

<?php include("template/header-main.php") ?>

<div class="section-register">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="register">
                    <h2>Регистрация на сайте</h2>
                    <p>Заполните поля формы</p>
                    <form class="form-group form-all f-prof validate_client_register">
                        <input type="hidden" name="Регистрация на сайте клиент">
                        <div class="recourse-lk">
                            <div class="item">
                                <p>Обращение:</p>
                            </div>
                            <div class="recourse-name">
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test" value="Господин" checked>
                                        <span class="radio-custom"></span>
                                        <span class="label">Господин</span>
                                    </label>
                                </div>
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test" value="Госпожа">
                                        <span class="radio-custom"></span>
                                        <span class="label">Госпожа</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="center-input client_register_center">
                            <div class="block-profile">
                                <div class="v-d">
                                    <input class="popup-call__name" type="text" name="name" placeholder="Имя">
                                </div>
                                <div class="v-d">
                                    <input class="popup-call__name" type="text" name="surname" placeholder="Фамилия">
                                </div>
                            </div>
                            <label class="all-label">Адрес:</label>
                            <div class="block-profile">
                                <div class="adress-pr val_adress">
                                    <div class="v-d-index">
                                        <input class="index" type="tel" name="index" placeholder="Индекс" maxlength="5">
                                    </div>
                                    <div class="v-d">
                                        <input class="city" type="text" name="city" placeholder="Город">
                                    </div>
                                </div>
                                <div class="v-d">
                                    <input class="adress-form" type="text" name="adress" placeholder="Адрес">
                                </div>
                            </div>
                            <label class="all-label">Контактные данные:</label>
                            <div class="block-profile">
                                <div class="v-d">
                                    <input class="popup-call__email" type="email" name="email" placeholder="Email">
                                </div>
                                <div class="v-d">
                                    <input class="form-control" placeholder="Телефон"
                                           name="phone" id="Piramida_phone-qust" type="tel" maxlength="20"/>
                                    <div class="help-block error" id="Piramida_phone_em_" style="display:none"></div>
                                    <input type="checkbox" id="phone_mask-qust"
                                           placeholder="Телефон" checked>
                                    <label id="descr" for="phone_mask2" class="phone-mask2">Использовать маску</label>
                                </div>
                            </div>
                            <label class="all-label">Пароль:</label>
                            <div class="block-profile password_validate">
                                <div class="v-d">
                                    <input id="password1" type="password" name="password_new" placeholder="Новый пароль">
                                </div>
                                <div class="v-d">
                                    <input id="password2" class="mob_style_validate"  type="password" name="password_repeat" placeholder="Повторить пароль">
                                </div>
                            </div>
                            <p id="validate-status"></p>
                            <div class="block-to-accept">
                                <div class="left-col">
                                    <p>Регистрируясь на сайте, я принимаю</p>
                                </div>
                                <div class="right-col">
                                    <a href="#js-terms-use">Пользовательское соглашение</a>
                                </div>
                            </div>
                            <div class="but-btn login-btn bot-btn btnSubmit">
                                <button>Регистрация</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/register_client/mainController.js"></script>
<script type="text/javascript" src="/js/register_client/validateController.js"></script>
<script type="text/javascript" src="/js/register_client/EventBut.js"></script>
<script type="text/javascript" src="/js/register_client/passwordMatching.js"></script>
<script type="text/javascript" src="/js/lk_other/validateNumber.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

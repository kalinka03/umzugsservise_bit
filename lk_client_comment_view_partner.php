<?php include("template/header-lk.php") ?>
<div class="section-lk-top">
    <div class="container">
            <div class="col-sm-12">
                <div class="come_back">
                    <a href="/lk_client_info_view_partner.php">Назад</a>
                </div>
            </div>
    </div>
    <div class="container">
        <div class="content-news">
            <div class="block-partner_information_top all_comment_partner">
                <div class="foto-partner">
                    <img src="img/lk_client_info_partner/page1/logo.png" alt="">
                </div>
                <div class="center-col">
                    <div class="company_name">
                        <div class="name">
                            <p>Название компании на случай если очень длинное название компании</p>
                        </div>
                        <div class="number">
                            <p>ID <span>2510603</span></p>
                        </div>
                    </div>
                    <div class="adress">
                        <p>13409 Bad Münster am Stein-Ebernburg</p>
                    </div>
                </div>
                <div class="right-col">
                    <div class="but-btn but-lk but_comment-partner">
                        <a href="/lk_client_order_done_comment.php">Оставить отзыв</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="overall overall_comment_partner">
                    <div class="overall-rating">
                        <div class="title">
                            <h2>
                                Общий рейтинг
                            </h2>
                        </div>
                        <div class="block-rating">
                            <div class="assessment-rating">
                                <div class="star">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                </div>
                                <div class="assessment">
                                    <p>4,85/5</p>
                                </div>
                            </div>
                            <div class="rev-company-comment">
                                <div class="rew">
                                    <p>Отзывов <span>114</span></p>
                                </div>
                                <div class="percent">
                                    <p>98<span>% положительных</span></p>
                                </div>
                            </div>
                            <div class="block-recommend">
                                <p><span>68 </span>рекомендуют</p>
                            </div>
                            <div class="block-no-recommend">
                                <p><span>2</span> не рекомендуют</p>
                            </div>
                        </div>
                    </div>
                    <div class="overall-block-company">
                        <div class="title">
                            <h2>
                                Общая оценка
                            </h2>
                        </div>
                        <div class="all-overall">
                            <div class="item">
                                <div class="l-col">
                                    <p>Цена:</p>
                                </div>
                                <div class="center-col">
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                                <div class="r-col">
                                    <p>4,85/5</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="l-col">
                                    <p>Качество выполнения заказа:</p>
                                </div>
                                <div class="center-col">
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                                <div class="r-col">
                                    <p>4,85/5</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="l-col">
                                    <p>Качество работы сотрудников:</p>
                                </div>
                                <div class="center-col">
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                                <div class="r-col">
                                    <p>5/5</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="l-col">
                                    <p>Качество обслуживания компании:</p>
                                </div>
                                <div class="center-col">
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                                <div class="r-col">
                                    <p>4,85/5</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all-rev-comment-partner">
                    <div class="some-review-company">
                        <div class="l-col">
                            <div class="block-info">
                                <div class="name">
                                    <h5>Имя Фамилия</h5>
                                    <p>50674 Кельн, Германия</p>
                                    <p>Добавлено <span>дд.мм.гггг</span></p>
                                </div>
                                <div class="more-info-company">
                                    <p>Цена:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество выполнения заказа:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество работы сотрудников:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество обслуживания компании:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="r-col">
                            <div class="text-review-company">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur
                                    ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                    orci,
                                    sed rhoncus sapien nunc eget.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur
                                    ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                    orci,
                                    sed rhoncus sapien nunc eget.</p>
                            </div>
                            <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                            <a class="close_reviews_company_partner" href="#">Скрыть</a>
                        </div>
                    </div>
                    <div class="some-review-company">
                        <div class="l-col">
                            <div class="block-info">
                                <div class="name">
                                    <h5>Имя Фамилия</h5>
                                    <p>50674 Кельн, Германия</p>
                                    <p>Добавлено <span>дд.мм.гггг</span></p>
                                </div>
                                <div class="more-info-company">
                                    <p>Цена:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество выполнения заказа:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество работы сотрудников:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество обслуживания компании:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <div class="block-recommend">
                                        <p>Pекомендую</p>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="r-col">
                            <div class="text-review-company">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. </p>
                            </div>
                            <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                            <a class="close_reviews_company_partner" href="#">Скрыть</a>
                        </div>
                    </div>
                    <div class="some-review-company">
                        <div class="l-col">
                            <div class="block-info">
                                <div class="name">
                                    <h5>Имя Фамилия</h5>
                                    <p>50674 Кельн, Германия</p>
                                    <p>Добавлено <span>дд.мм.гггг</span></p>
                                </div>
                                <div class="more-info-company">
                                    <p>Цена:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество выполнения заказа:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество работы сотрудников:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                    <p>Качество обслуживания компании:</p>
                                    <div class="star">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                        <img class="n-star" src="img/main/block7/7_star.svg" alt="">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="r-col">
                            <div class="text-review-company">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur
                                    ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                    orci,
                                    sed rhoncus sapien nunc eget.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur
                                    ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                    orci,
                                    sed rhoncus sapien nunc eget.
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet.
                                    Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                    pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur
                                    ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis
                                    orci,
                                    sed rhoncus sapien nunc eget.</p>
                            </div>
                            <a class="more_reviews_company_partner" href="#">Показать полностью</a>
                            <a class="close_reviews_company_partner" href="#">Скрыть</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-pagination">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pagination pag_commets_partner">
                                <a class="arrow-left" href="#"></a>
                                <a class="active number" href="#">1</a>
                                <a class="number" href="#">2</a>
                                <a class="number" href="#">3</a>
                                <a class="more" href="#">...</a>
                                <a class="number" href="#">7</a>
                                <a class="number" href="#">8</a>
                                <a class="arrow-right" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/openCommit.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>

function DownloadPhotoCompany() {
    this.addPhoto = function () {
        $('.logo-company-foto .border-foto  .add-plus').unbind('click');
        $('.logo-company-foto .border-foto  .add-plus').click(function () {
            $(this).parents('.border-foto').find('.img-grup:last-child input').click();
            binds();
            return false;
        });


        $('form').submit(function () {
            var $inps = $('.img-grup input');
            $inps.each(function () {
                var $this = $(this);
                if ($this[0].files.length == 0) {
                    $this.parents('.img-grup').remove();
                }
            });
        });

        var binds = function () {
            var $inps = $('.logo-company-foto .border-foto .img-grup input');
            var $close = $('.del-img');
            $close.unbind('click');
            $inps.unbind('change');
            $inps.change(function () {
                var $input = $(this);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $input.parent().find('img').attr('src', e.target.result);

                    if ($input[0].files.length !== 0)
                    $input.parents('.img-wrapp__none_img').removeClass('img-wrapp__none_img');
                    $input.parents('.border-foto').find('.add-plus').css('opacity', '0');
                };
                reader.readAsDataURL($input[0].files[0]);
            });
            $close.click(function () {
                $(this).parents('.img-grup ').addClass('img-wrapp__none_img');
                $(this).parents('.border-foto').find('.add-plus').css('opacity', '1');
            })
        }
    }
    this.uploadFile = function () {
        $('.table-col-s .file-upload').unbind('click');
        $('.table-col-s .file-upload').click(function () {
            // FileDownload = new UploadFile();
            $(this).parents('.wrapper-table-col').find("input[type=file]").change(function () {
                var file = $(this).val();
                var filename = file.replace(/.*\\/, "");
                console.log(filename);
                $(this).parents('.table-col-s').find("#filename").show();
                $(this).parents('.table-col-s').find("#filename").val(filename);
                $(this).parents('.table-col-s ').find(".file-upload").hide();
                $(this).parents('.wrapper-table-col ').find(".table-col-r a").css('display', 'inline');
            });

        });
    }
    this.deleteFile = function () {
        $('.table-col-r a').unbind('click');
        $('.table-col-r a').on('click', function (e) {
            e.preventDefault();
            $(this).parents('.wrapper-table-col').find('#filename').val('');
            $(this).parents('.wrapper-table-col').find(".file-upload").show();
            $(this).parents('.wrapper-table-col').find(".table-col-r a").hide();
            $(this).parents('.wrapper-table-col').find("#filename").hide();
        })
    }

    this.addPhoto();
    this.uploadFile();
    this.deleteFile();

}
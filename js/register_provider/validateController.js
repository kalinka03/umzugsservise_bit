function ValidateForm() {
    this.validate = function () {
        $("form").validate({
            ignore: '',
            rules: {
                name_company: {
                    required: true,
                },
                registration: {
                    required: true,
                },
                company_employees: {
                    required: true,
                },
                logo_company
                    : {
                    required: true,
                },
                file_upload: {
                    required: true,
                },
                name: {
                    required: true,
                },
                surname: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                phone:{
                    required: true,
                    regex: /([\+\-\(\)0-9]{11,16})\_{0,4}$/g
                },
                city: {
                    required: true,
                },
                index: {
                    required: true,
                    regex: /^([\(\)\+\- ]{0,2}[\d]){5,50}$/g
                },
                adress: {
                    required: true,
                },
                city_for: {
                    required: true,
                },
                index_for: {
                    required: true,
                },
                adress_for: {
                    required: true,
                },
                password_repeat:{
                    required: true,
                },
                password_new: {
                    required: true,
                    regex: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g,
                },
                "document_dounload[]":{
                    required: true,
                },
                "document_dounload_tabNext[]":{
                    required: true,
                }
            },
            messages: {
                name_company: {
                    required: "Заполните поле",
                },
                registration:  {
                    required: "Заполните поле",
                },
                company_employees: {
                    required: "Заполните поле",
                },
                logo_company:  {
                    required: "Заполните поле",
                },
               doc_upload: {
                    required: "Заполните поле",
                },
                name: {
                    required: "Заполните поле",
                },
                surname: {
                    required: "Заполните поле",
                },
                email: {
                    required: "Заполните поле",
                    email: "Введите  корректный адрес",
                },
                phone:{
                    required: "Заполните поле",
                    regex: "Заполните корректно номер",
                },
                city: {
                    required: "Заполните поле",
                },
                adress: {
                    required: "Заполните поле",
                },
                index: {
                    required: "Заполните поле",
                    regex: "Введите 5 цифр",
                },
                index_for: {
                    required: "Заполните поле",
                },
                city_for: {
                    required: "Заполните поле",
                },
                adress_for: {
                    required: "Заполните поле",
                },
                password_new:{
                    required: "Введите пароль",
                    regex: "Минимум 8 символов, буквы разного региста, цифры",
                },
                password_repeat: {
                    required: "Повторите пароль"

                },
                "document_dounload[]": {
                    required: "Загрузите файл"
                },
                "document_dounload_tabNext[]": {
                    required: "Загрузите файл"
                }

            }
        })




    }



    this.next = function () {
        $('.btnNextprov').on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: 0}, 500);
            if (!$(".tab1_register :valid").valid()) return false;
            if ($(".tab2_register").is(":visible")) {
                if (!$(".tab2_register :valid").valid()) return false;
            }
            $(this).parents('.content-choice').removeClass('active').next().addClass('active');
            $('html, body').animate({scrollTop: 0},500);
            $(this).parents('.register-provider').find('.step-menu button.active').addClass('passed_stage').removeClass('active').next().removeClass('passed_stage').addClass('active');

        });
    };
    this.submit = function () {
        $('.btnSubmit').on('click', function () {
            if(!$(".tab3_register  :valid").valid()) return false;
            $('html, body').animate({scrollTop: 0},500);
            $('.btnSubmit').submit();
        })
    }

    this.next();
    this.submit();
    this.validate();

}
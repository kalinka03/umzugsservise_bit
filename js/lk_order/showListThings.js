function showListThings() {
    this.createTemplateItem = function () {
        this.create = function () {
            $('.list_all_js').remove();
            allData = [];
            var object = $('.content-choice-things');
            object.each(function () {
                allData.push({
                    name: $(this).find('.nameThings').text(),
                    key: $(this).attr('data'),
                    id: $(this).attr('data'),
                    list: [],
                    other: []
                });
                $(this).find('.things_one').each(function () {
                    allData[allData.length - 1].list.push({
                        someNameThings: $(this).find('.r-col h5').text(),
                        parametrMmultiplication: $(this).find('.r-col input[name = multiplication_all]').val(),
                        ThingImg: $(this).find('.l-col img').attr('src'),
                        ParametrThings: $(this).find('.r-col input[id = rez_ar]').val(),
                        ParametrThingsHover: $(this).find('.r-col input[id = rez_delimiter]').val(),
                        ParamArea: $(this).find('.r-col input[name = things_area]').val(),
                        CounterThings: $(this).find('.counter input[id = count_parametr]').val(),
                        CommitThings: $(this).find('.right-col .r-all textarea').val(),
                        areaThings: $(this).find('.r-col input[name=rezult_areas]').val(),
                        gallery: {
                            id: $(this).find('.all-foto-block .arhive-foto').attr('data-id'),
                            GalleryThings: $(this).find('.arhive-foto .foto-block .img-grup .img-wrapp img')
                        }
                    });

                });
                $(this).find('.things_other').each(function () {
                    allData[allData.length - 1].other.push({
                        someNameOtherThings: $(this).find('.r-col input[id = nameOther]').val(),
                        parametrMmultiplication: $(this).find('.r-col input[name = multiplication]').val(),
                        ParametrOtherThings: $(this).find('.r-col input[id = rez_ar_other]').val(),
                        ParametrOtherThingsHover: $(this).find('.r-col input[id = rez_delimiter_other]').val(),
                        ParamArea: $(this).find('.r-col input[name = things_area]').val(),
                        CounterThings: $(this).find('.counter input[id = count_parametr]').val(),
                        CommitThings: $(this).find('.right-col .r-all textarea').val(),
                        areaThings: $(this).find('.r-col input[name=rezult_areas]').val(),
                        gallery: {
                            id: $(this).find('.all-foto-block .arhive-foto').attr('data-id'),
                            GalleryThings: $(this).find('.arhive-foto .foto-block .img-grup')
                        }
                    });
                });
            });
            console.log(allData);
            allData.forEach(function (elem) {
                if (elem.list.length > 0 || elem.other.length > 0) {
                    $('.completed_things').append(
                        '<div class="list_all_js">' +
                        '<h2>' + elem.name + '</h2>' +
                        '<div class="list-things" id="' + elem.key + '">' +
                        '</div>' +
                        '<div class="line_shadow"></div>' +
                        '</div>');
                    ;
                }
                elem.list.forEach(function (list) {
                    if (list !== -1) {
                        $('#' + elem.key).append(
                            '<div class="item">' +
                            '<div class="l-col">' +
                            '<img src="' + list.ThingImg + '"  alt="">' +
                            '</div>' +
                            '<div class="center-col">' +
                            '<h5>' + list.someNameThings + '<span>x' + list.CounterThings + '</span></h5>' +
                            '<input  type="hidden" name="list_area" value="' + list.ParamArea + '">' +
                            '<input  type="hidden" name="param_area" value="' + list.ParametrThingsHover + '">' +
                            '<input  type="hidden" name="things_area" value="' + list.ParametrThingsHover * list.CounterThings+ '">' +
                            '<p class="parametr_specify">' + list.ParametrThings  + ' </p>' + '<span>м<sup>3</sup></span> ' +
                            '<p class="parametr_all_specify">' + list.parametrMmultiplication + '<span></span></p>' +
                            '</div>' +
                            '<div class="r-col">' +
                            '<p>' + list.CommitThings + '</p>' +
                            '<div class="gallery-foto">' +
                            '<div class="item" id="' + list.gallery.id + elem.key + '">' +
                            // '<img src="'+ list.gallery.GalleryThings+'" alt="">'+
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>')

                        if ($('.arhive-foto .foto-block .img-grup ').is(':visible')) {

                            $('#' + list.gallery.id + elem.key).append(

                                list.gallery.GalleryThings
                            )
                             // list.gallery.GalleryThings.attr('src').appendTo('#' + list.gallery.id + elem.key +' img');
                        }
                    }
                });
                elem.other.forEach(function (other) {
                    if (other !== -1) {
                        $('#' + elem.key).append(
                            '<div class="item">' +
                            '<div class="l-col">' +
                            '<img src="../img/order-relocation/tabs3/furniture/18Zaglushka.svg"  alt="">' +
                            '</div>' +
                            '<div class="center-col">' +
                            '<h5>' + other.someNameOtherThings + '<span>x' + other.CounterThings + '</span></h5>' +
                            '<input  type="hidden" name="list_area" value="' + other.ParamArea + '">' +
                            '<input  type="hidden" name="param_area" value="' + other.ParametrOtherThingsHover + '">' +
                            '<input  type="hidden" name="things_area" value="' + other.ParametrOtherThingsHover * other.CounterThings+ '">' +
                            '<p class="parametr_specify">' + other.ParametrOtherThings + ' </p>' + '<span>м<sup>3</sup></span> ' +
                            '<p class="parametr_all_specify">' + other.parametrMmultiplication + '<span></span></p>' +
                            '</div>' +
                            '<div class="r-col">' +
                            '<p>' + other.CommitThings + '</p>' +
                            '<div class="gallery-foto">' +
                            '<div class="item" id="' + other.gallery.id + elem.key + '">' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                        if ($('.arhive-foto .foto-block .img-grup').is(':visible')) {
                            $('#' + other.gallery.id + elem.key).append(

                                other.gallery.GalleryThings
                            )
                        }
                    }
                })
            });
        }
        this.create();
    }
    Area = new showAreaThings();
    this.createTemplateItem();
}
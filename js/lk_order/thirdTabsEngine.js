function ThirdTabEngine() {
    this.currentActiveTab = false;
    (function () {
        $(".list-things-bt").on("click", function () {
            CallThirdTab.currentActiveTab = $(this).attr("id");
        });
    })();


    this.fileControl = function () {
        $(".item.open_foto").on('click', (function () {
            event.preventDefault();
            $('.list').removeClass('border-green-active');
            $(this).parents('.list').addClass('border-green-active');
            $('.item.open_download_file').removeClass('border-green-active');
            $(".downloading-files .list .hover-downloading-foto").show();
            $(".hover-downloading-file").hide();
            $('.file-upload input[type=file]').removeAttr('disabled');
            if ($('.hover-downloading-foto-sk').is(':visible')) {
                $('.submission-sk').show();
            }
            var fileopen = $(this).parents('.list-things-block').find(".open_download_file .file-upload input[type=file]");
            fileopen.replaceWith('<input type="file" name="file">');
        }));
    };

    Addfoto = new DownloadPhoto();
    this.uploadFile = function () {
        $('.open_download_file').click(function () {
            ConfirmDimensionsGlobal = true;
            $(".file-upload input[type=file]").unbind('change');
            $('.submission-sk').show();
            $(this).parents('.list').find("#filename").val('');
            $('.list').removeClass('border-green-active');
            $(this).parents('.list').addClass('border-green-active');
            $(".downloading-files .list .hover-downloading-foto").hide();

            $(".file-upload input[type=file]").change(function () {
                $(this).parents('.list').find(".hover-downloading-file").show();
                var file = $(":file[name=file]").val();
                var filename = file.replace(/.*\\/, "");
                $(this).parents('.list').find("#filename").val(filename);
                $('.file-upload input[type=file]').attr('disabled', true);
                return false;
            });
        });
    }
    this.deleteFile = function () {
        var $close = $('.del-file');
        $close.click(function () {
            var fileopen = $(this).parents('.list').find(".file-upload input[type=file]");
            fileopen.replaceWith('<input type="file" name="file">');
            $(this).parents('.file-block').find("#filename").val('');
            $('.hover-downloading-file').hide();
            $('.file-upload input[type=file]').removeAttr('disabled');
        })
    }


    this.showResult = function () {
        $(".choice_paragraph").on('click', function (event) {
            event.preventDefault();
            if(ConfirmDimensionsGlobal){
                if ($(".addThings").is(":visible")) {
                    if (!$(".addThings :valid").valid()) return false;
                }

                showTab1();
                showTab2();
                showTab3();
                $('html, body').animate({scrollTop: 0}, 500);
            }else{
                ConfirmDimensionsGlobal = true;
            }


    }
)
    ;
    var showTab1 = function () {
        if ($('.create-things').is(":visible")) {
            $('.complete_checking_things').show();
            ListThings = new showListThings();
            AreaThings = new showAreaThings();
            $('.create-things').hide();
        }
    }
    var showTab2 = function () {
        if (($('.hover-downloading-foto').is(":visible")) && ($('.img-grup').is(':visible'))) {
            $('.full_photo_check').show();
            $('.choice-block-things').hide();
            $('.complete_checking_things').hide();
            $('.list_all_js').hide();
            ListPhoto = new showListPhoto();
        }
    }

    var showTab3 = function () {
        if ($('.hover-downloading-file').is(":visible")) {
            $('.full_download_check').show();
            $('.choice-block-things').hide();
            $('.download_file').text($(".fileDownload input").val());
            var string = $('.download_file').text();
            var ext = string.split('.').pop();
            if (ext == 'doc' || ext == 'docx') {
                $(".picture-document").html('<img src="/img/order-relocation/tabs3/files/doc.svg">');
            }
            if (ext == 'pdf') {
                $(".picture-document").html('<img src="/img/order-relocation/tabs3/files/pdf.svg">');
            }
            if (ext == 'xls' || ext == 'xlsx') {
                $(".picture-document").html('<img src="/img/order-relocation/tabs3/files/xls.svg">');
            }
            $('.complete_checking_things').hide();
            $('.list_all_js').hide();
        }
    }

}

    this.clearOtherInput = function () {

        $(".submission-btn").on('click', function (e) {
            e.preventDefault();
            if (CallThirdTab.currentActiveTab == '1') {
                clearTab2();
                clearTab3();
                Validate
            }
            ;
            if (CallThirdTab.currentActiveTab == '2') {
                clearTab1();
                clearTab3();
                Validate
            }
            ;
            if (CallThirdTab.currentActiveTab == '3') {
                clearTab1();
                clearTab2();
                Validate
            }
        });
    };

    var clearTab1 = function () {
        $('.addThings .some-things ').remove();
    }

    var clearTab2 = function () {
        $('.hover-downloading-foto .foto-block .img-grup').remove();
        $('.hover-downloading-foto').hide();

    }
    var clearTab3 = function () {
        $('.hover-downloading-file .file-block .fileDownload').remove();
        $('.hover-downloading-file').hide();
    }


    this.clearOtherInput = function () {

        $(".submission-btn").on('click', function (e) {
            e.preventDefault();
            if (CallThirdTab.currentActiveTab == '1') {
                clearTab2();
                clearTab3();
                Validate
            }
            ;
            if (CallThirdTab.currentActiveTab == '2') {
                clearTab1();
                clearTab3();
                Validate
            }
            ;
            if (CallThirdTab.currentActiveTab == '3') {
                clearTab1();
                clearTab2();
                Validate
            }
        });
    };

    var clearTab1 = function () {
        $('.addThings .some-things ').remove();
    }

    var clearTab2 = function () {
        $('.hover-downloading-foto .foto-block .img-grup').remove();
        $('.hover-downloading-foto').hide();

    }
    var clearTab3 = function () {
        $('.hover-downloading-file .file-block .fileDownload').remove();
        $('.hover-downloading-file').hide();
    }


    this.fileControl();
    this.uploadFile();
    this.deleteFile();
    this.showResult();
    this.clearOtherInput();


}








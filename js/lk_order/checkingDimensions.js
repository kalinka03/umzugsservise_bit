function ConfirmDimensions(item) {
    this.element = item.find(".one_thing_parametr");
    var main = this;
    ConfirmDimensionsGlobal = true;
    this.confirmDimensions = function () {
        var ret = true;

        if (this.element.is(":visible")) {
            var l = $(this.element).find('.col-dim input[id=l]').val();
            var h = $(this.element).find('.col-dim input[id=h]').val();
            var v = $(this.element).find('.col-dim input[id=v]').val();
            if ((l.length !== 0) || (h.length  !== 0) || (v.length !== 0)) {
                $(this.element).parents(".r-col").find(".confirm_meanings").show();
                ret = false;
            } else {
                $(this.element).parents(".r-col").find(".confirm_meanings").hide();
            }
        }
        return ret;
    };

    $(".choice_paragraph, .btnNext_sklad").bindFirst('click',function (e) {
        if (!main.confirmDimensions()) {
            ConfirmDimensionsGlobal = false;
        }
    });


}
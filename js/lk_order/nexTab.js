function NexTab() {
    ValidatePages = new formsAllPages();
    this.next = function () {
        $('.btnNext').on('click', function (e) {
            e.preventDefault();
            if(!$(".geography-block  :valid").valid()) return false;
            if ($(".conditions-block").is(":visible")) {
                if (!$(".conditions-block :valid").valid()) return false;
            }
            $(this).parents('.content-choice').removeClass('active').next().addClass('active');
            $('html, body').animate({scrollTop: 0}, 500);
            $(this).parents('.section-choice').find('.nav-tabs button.active').removeClass('active').next().addClass('active');

        });
    };
    this.next();
}
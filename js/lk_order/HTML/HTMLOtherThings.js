function ThingsOtherTemplates() {

    this.thingOther = '<div class="some-things things_other"> ' +
        '<div class="delete-block clearfix"> ' +
        '<div class="delete-things">  </div>' +
        '</div>' +
        '<div class="information-things"> ' +
        '<div class="left-col">' +
        '<div class="r-col">' +
        '<div class="r-all">' +
        '<label class="all-label">Название</label>' +
        ' <input id="nameOther" name="qty[]" class="popup-picking__text" rows="3" name="big_list_other[][name_things]" placeholder="Введите название"/>' +
        '<input  type="hidden" name="rezult_other" >' +
        '<input  type="hidden" name="rezult_areas" >' +
        '</div>' +
        '<input  type="hidden" name="rezult_hid" value="0">' +
        '<input  type="hidden" name="rezult_all" value="0">' +
        '<input  id="rez_delimiter_other" type="hidden" name="rez_delimiter_other" >' +
        '<input  id="rez_ar_other" type="hidden" name="rez_ar_other">' +
        '<input  disabled="disabled" id="parametr_specify_other" name="list_parametr"  class="parametr_value" value="">'+ '<span class="measurements">м<sup>3</sup></span>' +
        ' <div class="specify-dimensions other_spec_dim block-active-things">' +
               '<div class="col-dim">' +
        '<label class="all-label">L, м</label>' +
        '<input id="l_other" class="l-param parametr_value_other" type="number" step="0.01" min="0" AUTOCOMPLETE="off" name="l_other[]">' +
        '</div>' +
        '<div class="col-dim">' +
        '<label class="all-label">H, м</label>' +
        '<input id="h_other" class="h-param parametr_value_other" type="number" step="0.01" AUTOCOMPLETE="off" min="0" name="h_other[]" >' +
        '</div>' +
        '<div class="col-dim">' +
        '<label class="all-label">B, м</label>' +
        '<input id="v_other" class="v-param parametr_value_other" type="number" step="0.01" min="0" AUTOCOMPLETE="off"  name="v_other[]">' +
        '</div>' +
        '</div>' +
        '<input  type="hidden" name="multiplication" >' +
        '<input  type="hidden" name="multiplication_rez" >' +
        '<div class="counter not-name-count">' +
        '<span class="down"></span>' +
        '<input id="count_parametr" type="text" value="1"/>' +
        ' <span class="up"></span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="right-col">' +
        '<div class="r-all">' +
        '<label class="all-label">Комментарий:</label>' +
        ' <textarea class="popup-picking__text" rows="3"   name="big_list_other[][desc]" placeholder="Комментарий"></textarea>' +
        '</div>' +
        ' </div>' +
        '</div>' +
        '<div class="all-foto-block">' +
        '<div class="add-foto">' +
        '<div class="butt">' +
        '<a href="#" class="uploadButton">Добавить фото</a>' +
        '</div>' +
        '<div class="close-foto-things"></div>' +
        '</div>' +
        '<div class="arhive-foto" data-id="<% this.id %>">' +
        '<div class="foto-block">' +
        '<div class="new-foto add-plus">' +
        '<label for="uploadbtn" class="uploadButton"></label>' +
        '<input style="opacity: 0; z-index: -1;" type="file" name="big_list_other[][upload]" id="uploadbtn">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="line_shadow">' +
        '</div>' +
        '</div>' ;
}
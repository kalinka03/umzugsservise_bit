function ThisTemplates() {

    this.things = '<div class="some-things things_one"> ' +
        '<div class="delete-block clearfix"> ' +
        '<div class="delete-things">  </div>' +
        '</div>' +
        '<div class="information-things"> ' +
        '<div> </div>' +
        '</div>' +
        '<div class="information-things"> ' +
        '<div class="left-col">' +
        '<div class="l-col">' +
        '<img src="<% this.img %>"  alt="">' +
        '<input  type="hidden" name="big_list_gostina[][name_things]" >' +
        '</div>' +
        '<div class="r-col">' +
        '<h5><% this.name %></h5>' +
        '<input  type="hidden" name="big_list_gostina[][name_things]" >' +
        '<input  type="hidden" name="rezult_all" value="0.86">' +
        '<input  type="hidden" name="rezult_hid" value="0.86">' +
        '<input  id="rezult_areas" type="hidden" name="rezult_areas" value="0.86" >' +
        '<input  id="rez_delimiter" type="hidden" name="rez_delimiter" value="0.86" >' +
        '<input  id="rez_ar" type="hidden" name="rez_ar" value="0,86" >' +
        '<input    disabled="disabled" size="number" id="parametr_specify" name="list_parametr"  class="parametr_value"  value="0,86">'+ '<span>м<sup>3</sup></span>' +
        '<div class="parametr">' +
        '<p>Уточнить габаритные размеры</p>' +
        '<input  type="hidden" name="multiplication" >' +
        '<input  type="hidden" name="multiplication_rez" >' +
        '<input  type="hidden" name="multiplication_all" >' +
        '</div>' +
        '<input  type="hidden" name="things_area"  value="0,86">' +
        '<div class="specify-dimensions one_thing_parametr">' +
        '<div class="col-dim">' +
        '<label class="all-label">L, м</label>' +
        '<input   id="l" class="l-param parametr_value" type="number"    step="0.01" min="0"  name="big_list_gostina[][number]" >' +
        '</div>' +
        '<div class="col-dim">' +
        '<label class="all-label">H, м</label>' +
        '<input id="h" class="h-param parametr_value" type="number"      step="0.01" min="0"  name="big_list_gostina[][number]" >' +
        '</div>' +
        '<div class="col-dim">' +
        '<label class="all-label">B, м</label>' +
        '<input  id="v" class="v-param parametr_value" type="number"   step="0.01" min="0"  name="big_list_gostina[][number]" >' +
        '</div>' +
        '<div class="add-dim add-things"></div>' +
        '<div class="del-dim del-things"></div>' +
        '</div>' +
        '<p class="confirm_meanings">Подтвердите действие</p>'+
        '<div class="counter">' +
        '<span class="down"></span>' +
        '<input id="count_parametr"  type="text" name="big_list_gostina[][number]" value="1"/>' +
        '<span class="up"></span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="right-col">' +
        '<div class="r-all">' +
        '<label class="all-label">Комментарий:</label>' +
        ' <textarea class="popup-picking__text" rows="3"   name="big_list_gostina[][desc]" placeholder="Комментарий"></textarea>' +
        '</div>' +
        ' </div>' +
        '</div>' +
        '<div class="all-foto-block">' +
        '<div class="add-foto">' +
        '<div class="butt">' +
        '<a href="#" class="uploadButton">Добавить фото</a>' +
        '</div>' +
        '<div class="close-foto-things"></div>' +
        '</div>' +
        '<div class="arhive-foto" data-id="<% this.id %>">' +
        '<div class="foto-block">' +
        '<div class="new-foto add-plus">' +
        '<label for="uploadbtn" class="uploadButton"></label>' +
        '<input style="opacity: 0; z-index: -1;" type="file" name="big_list_other[][upload]" id="uploadbtn">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="line_shadow">' +
        '</div>' +
        '</div>';
}
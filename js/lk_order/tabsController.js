function TabEngine1() {
    Validate = new ValidateForm();
    this.uiDesign = function () {
        $('.change_order').change(function () {
            if ($(this).val() === '2') {
                $(".exact-date").show();
                $(".date-range").hide();
            }
            if ($(this).val() === '3') {
                $(".date-range").show();
                $(".exact-date").hide();
            }
            if ($(this).val() === '1') {
                $(".date-range").hide();
                $(".exact-date").hide();
            }
        });
    };
    this.uiDesign();
}

function TabEngine2() {
    Validate = new ValidateForm();
    this.uiDesign = function () {
        $("input.add_service").on('change', (function () {
            $(".hover-service").toggle();
            $('.add-serv-descript').toggle();
        }));
    };

    this.prev = function () {
        $('.btnPrevious').click(function () {
            event.preventDefault();
            $(this).parents('.section-choice').find('.nav-tabs button.active').removeClass('active').prev().addClass('active');
            $('html, body').animate({scrollTop: 0}, 500);
        });
    };
    this.uiDesign();
    this.prev();

}


function TabEngine3() {
    this.svgImg = function () {
        $.fn.toSVG = function (options) {
            var params = $.extend({
                svgClass: "replaced-svg",
                onComplete: function () {
                },
            }, options)
            this.each(function () {
                var $img = jQuery(this);
                var imgID = $img.attr('id');
                var imgClass = $img.attr('class');
                var imgURL = $img.attr('src');
                if (!(/\.(svg)$/i.test(imgURL))) {
                    console.warn("image src='" + imgURL + "' is not a SVG, item remained tag <img/> ");
                    return;
                }
                $.get(imgURL, function (data) {
                    var $svg = jQuery(data).find('svg');
                    if (typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    if (typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass + " " + params.svgClass);
                    }
                    $svg = $svg.removeAttr('xmlns:a');
                    if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                    }
                    $img.replaceWith($svg);
                    typeof params.onComplete == "function" ? params.onComplete.call(this, $svg) : '';
                })
            });
        };
        $('.item img.svg').toSVG();
    };


    this.eventEngine = function () {
        $(".create_list_js").unbind('click');
        $(".create_list_js").click(function (event) {
            event.preventDefault();
            $(".create-things").show();
            $(".downloading-files").hide();
            $(".js_things_info").hide();
            $(".choice-block-things").hide();
            if ($('.hover-downloading-foto-sk').is(':visible')) {
                $('.submission-sk').show();
            }
        })


        $(".catalogEngine").on('click', (function (event) {
            event.preventDefault();
            $(".confirm_meanings").hide();
            $(".create-things").hide();
            $(".downloading-files").show();
            $(".js_things_info").show();
            $(".choice-block-things").show();

        }))
        $(".btnChangeThings").on('click', function (event) {
            event.preventDefault();
            $(".confirm_meanings").hide();
            if ($('.complete_checking_things').is(":visible")) {
                $('.create-things').show();
                $('.complete_checking_things').hide();
                $('html, body').animate({scrollTop: 0}, 500);
            }
            else {
                $('.choice-block-things').show();
                $('.full_photo_check').hide();
                $('.full_download_check').hide();
            }
        })
    }

    this.eventEngine();
    this.svgImg();
}


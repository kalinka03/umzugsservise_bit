function TabThirdListEngine() {
    // Filter = new OneFilterThing();
    this.tabOneTabs = function () {
        $('.tab-content-things>div:not(":first-of-type")').hide();
        $('.tab-menu-things button').each(function (i) {
            $(this).attr('data-tab', 'tab' + i);

        });
        $('.tab-content-things .content-choice-things').each(function (i) {
            $(this).attr('data-tab', 'tab' + i);
        });
        $('.tab-menu-things button').on('click', function (e) {
            e.preventDefault();
            var dataTab = $(this).data('tab');
            var getWrapper = $(this).closest('.choise-catalog-things');
            getWrapper.find('.tab-menu-things button').removeClass('active');
            $(this).addClass('active');
            getWrapper.find('.tab-content-things>.content-choice-things').hide();
            getWrapper.find('.tab-content-things>.content-choice-things[data-tab=' + dataTab + ']').show();
                Filter = new OneFilterThing();
            if ($(".addThings").is(":visible")) {
                if (!$(".addThings :valid").valid()) return false;
            }

        });
    };

    this.filter = function(){
        $('#things_filter_show').click(function() {
            Filter = new OneFilterThing();
        })
    }


    this.listCatalog = function () {
        $('.btnPrevious').click(function () {
            event.preventDefault();
            $(this).parents('.content-choice').removeClass('active').prev().addClass('active');
            $(this).parents('.section-choice').find('.nav-tabs button.active').removeClass('active').prev().addClass('active');
        });
    };


    this.getThings = function () {
        $('.content-choice-things .other-things .things .OneThing').on('click', (function (e) {
            e.preventDefault();
            $('.add_things_js button').prop('disabled',false);
            $('#things_filter_show').val('');

            var elemContentTab = $(this).parents('.content-choice-things').find('.addThings');
            var nameThings = $(this).attr('value');
            var imgThings = $(this).attr('src');
            var idThings = $(this).attr('data-id');
            TemplateEngineEvents(ListEngine.template.things,{
                img: imgThings,
                name: nameThings,
                id: idThings
            },elemContentTab );
            $('.choise-catalog-things .tab-content-things .content-choice-things .other-things').hide();

            new EventNewThing (elemContentTab.find('.things_one:last-child'));
            $('.content-choice-things .other-things .things .OneThing').show();
            if($('.all-quantity-things').is(':visible')){
                AreaSk = new showAreaSkThings();
            }


        }))
        $('.content-choice-things .other-things .things .other-add').on('click', (function (e) {
            e.preventDefault();
            $('.add_things_js button').prop('disabled',false);
            var elemContentOtherTab = $(this).parents('.content-choice-things').find('.addThings');
            var idThingsOther = $(this).attr('data-id');
            TemplateOtherEngineEvents(ListEngine.templateOther.thingOther, {
                id: idThingsOther
            },elemContentOtherTab);
            $('.choise-catalog-things .tab-content-things .content-choice-things .other-things').hide();

            // EventThing = new EventNewThing ();
            new EventNewThing (elemContentOtherTab.find('.things_other:last-child'));
        }))
    };


    this.filter();
    this.getThings();
    this.listCatalog();
    this.tabOneTabs();
    this.template = new  ThisTemplates();
    this.templateOther = new ThingsOtherTemplates();

}













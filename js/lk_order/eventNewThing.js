function EventNewThing(item) {
    this.deleteItem = function () {
        item.find('.delete-block').click(function () {
            $(this).parent('.some-things').remove();
            if (($(".hover_elements").length>0)) {
                AreaSk = new showAreaSkThings();
            }
        });

    }
    this.counter = function () {
        item.find('.down').click(function () {


            var $input = $(this).parent('.counter').find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();

            if ($(this).parents('.things_one').is(':visible')) {
                var $areaThings = $(this).parents('.r-col').find('#rez_delimiter').val();
                var querty = $(this).parents('.r-col').find('#count_parametr').val();
                var ar = $areaThings * querty;
                $(this).parents('.r-col').find('input[name=rezult_all]').val(ar);
                if (($(".hover_elements").length>0)) {
                    AreaSk = new showAreaSkThings();
                }
            }
            if ($(this).parents('.things_other').is(':visible')) {
                var $areaThingsOther = $(this).parents('.r-col').find('#rez_delimiter_other').val();
                var quertyOther = $(this).parents('.r-col').find('#count_parametr').val();
                var arOther = $areaThingsOther * quertyOther;
                $(this).parents('.r-col').find('input[name=rezult_all]').val(arOther);
                if (($(".hover_elements").length>0)) {
                    AreaSk = new showAreaSkThings();
                }
            }
            if (($(".hover_elements").length>0)) {
                AreaSk = new showAreaSkThings();
            }
            return false;
        });
        item.find('.up').click(function () {
            var $input = $(this).parent('.counter').find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            if ($(this).parents('.things_one').is(':visible')) {
                var $areaThings = $(this).parents('.r-col').find('#rez_delimiter').val();
                var querty = $(this).parents('.r-col').find('#count_parametr').val();
                var ar = $areaThings * querty;
                $(this).parents('.r-col').find('input[name=rezult_all]').val(ar);
                if (($(".hover_elements").length>0)) {
                    AreaSk = new showAreaSkThings();
                }
            }
            if ($(this).parents('.things_other').is(':visible')) {
                var $areaThingsOther = $(this).parents('.r-col').find('#rez_delimiter_other').val();
                console.log($areaThingsOther );
                var quertyOther = $(this).parents('.r-col').find('#count_parametr').val();
                var arOther = $areaThingsOther * quertyOther;
                $(this).parents('.r-col').find('input[name=rezult_all]').val(arOther);
                if (($(".hover_elements").length>0)) {
                    AreaSk = new showAreaSkThings();
                }
            }
            return false;
        });
    };

    this.showAddButton = function () {
        $('.add_things_js').on('click', (function (event) {
            event.preventDefault();
            $(this).parents('.content-choice-things').find('.other-things').show();
        }));
    };

    this.specify_dimensions = function () {
        item.find(".parametr").on('click', (function (event) {
            event.preventDefault();
            $(this).parents('.r-col').find('.parametr').hide();
            $(this).parents('.information-things').find('.left-col .r-col .specify-dimensions').css('display', 'flex');
                $('.parametr_value').blur(function () {
                    var price = +$(this).val();
                    $(this).val(price.toFixed(2));
                })
        }))
            $('.parametr_value_other').blur(function () {
                var price = +$(this).val();
                $(this).val(price.toFixed(2));
            })


        $(".del-things").on('click', function () {
            $('.some-things .information-things .left-col .r-col .specify-dimensions').hide();
            $(this).parents('.r-col').find('.col-dim input[name=l]').val('');
            $(this).parents('.r-col').find('.col-dim input[name=h]').val('');
            $(this).parents('.r-col').find('.col-dim input[name=v]').val('');
            $(this).parents('.r-col').find('.parametr').show();
        });
    }

    this.eventPhoto = function () {
        item.find(".add-foto .butt").on('click', function (event) {
            event.preventDefault();
            $(this).parents().siblings('.arhive-foto').show();
            $(this).parent().find('.close-foto-things').show();
        });
        item.find(".close-foto-things").on('click', function () {
            $(this).parents('.all-foto-block').find('.arhive-foto').hide();
            $(this).parents('.all-foto-block').find('.close-foto-things').hide();
        });
    }


    this.addPhoto = function () {
        $('.arhive-foto .foto-block .add-plus').unbind('click');
        $('.arhive-foto .foto-block .add-plus').click(function () {
            $(this).parents('.foto-block').append('<div class="img-grup  img-wrapp__none_img">\n' +
                '                               <div class="img-wrapp">\n' +
                '                                   <input type="file" style="display: none;"  name="gallery[]" accept="image/jpeg,image/png,image/jpg">' +
                '                                   <span><span class="clouse-bg"></span></span>\n' +
                '                                   <img src="">\n' +
                '                               <div class="del-img">\n' +
                '                               </div>\n' +
                '                               </div>\n' +
                '                           </div>');
            $(this).parents('.foto-block').find('.img-grup:last-child input').click();
            binds();
            return false;
        });

        $('form').submit(function () {
            var $inps = $('.img-grup input');
            $inps.each(function () {
                var $this = $(this);
                if ($this[0].files.length == 0) {
                    $this.parents('.img-grup').remove();
                }
            });
        });

        var binds = function () {
            var $inps = $('.arhive-foto .foto-block .img-grup input');
            var $close = $('.del-img');
            $close.unbind('click');
            $inps.unbind('change');
            $inps.change(function () {
                var $input = $(this);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $input.parent().find('img').attr('src', e.target.result);

                    if ($input[0].files.length !== 0)
                        $input.parents('.img-wrapp__none_img').removeClass('img-wrapp__none_img');
                };
                reader.readAsDataURL($input[0].files[0]);
            });
            $close.click(function () {
                $(this).parents('.img-grup').remove();
            })
        }
    }
    CheckingDim = new ConfirmDimensions(item);

    AreaOne = new EventAreaThingOne();
    AreaOther = new EventAreaThingOther();
    this.addPhoto();
    this.eventPhoto();
    this.specify_dimensions();
    this.showAddButton();
    this.deleteItem();
    this.counter();

}
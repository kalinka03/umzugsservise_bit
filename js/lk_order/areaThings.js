function showAreaThings() {
    this.Summ = function () {
        Area = [];
        var area_param = $('.list-things .center-col input[name=things_area]');
              area_param.each(function () {
            Area.push({
                value: $(this).val(),
            });
        });
        summ = 0;
        Area.forEach(function (param) {
            summ = summ + parseFloat(param.value);
        });
        summ_parse = (parseInt(summ * 100)) / 100;
        summ_area = summ_parse.toFixed(2);
        $('.all-quantity-things #area_things').val(summ_area);
        var area = $('.all-quantity-things  #area_things').val();
        var rez_area = area.replace('.', ',');
        $('.all-quantity-things h2 span.quantity-things').text(rez_area);
    }
    this.Summ();
}


function ValidateForm() {
    this.validate = function () {
        $("form").validate({
            rules: {
                name: {
                    required: true,
                },
                surname: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                phone:{
                    required: true,
                    regex: /([\+\-\(\)0-9]{11,16})\_{0,4}$/g,
                },
                password_new:{
                    required: true,
                    regex: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g,
                },
                city: {
                    required: true,
                },
                index: {
                    required: true,
                    regex: /^([\(\)\+\- ]{0,2}[\d]){5,50}$/g
                },
                adress: {
                    required: true,
                },
                city_for: {
                    required: true,
                },
                index_for: {
                    required: true,
                },
                adress_for: {
                    required: true,
                },
                order: {
                    required: true,
                },
                date_from: {
                    required: true,
                },
                date_range: {
                    required: true,
                },
                area: {
                    required: true,
                },
                pay_order: {
                    required: true,
                },
                room_type: {
                    required: true,
                },
                distance: {
                    required: true,
                },
                elevator: {
                    required: true,
                },
                number_floors: {
                    required: true,
                },
                number_room: {
                    required: true,
                },
                number_people: {
                    required: true,
                },
                transport: {
                    required: true,
                },
                r_type: {
                    required: true,
                },
                distance_parking: {
                    required: true,
                },
                presence_elevator: {
                    required: true,
                },
                n_floor: {
                    required: true,
                },
                means_transport: {
                    required: true,
                },
                password_repeat:{
                    required: true,
                }
            },
            messages: {
                name: {
                    required: "Заполните поле",
                },
                surname: {
                    required: "Заполните поле",
                },
                email: {
                    required: "Заполните поле",
                    email: "Введите корректный email"
                },
                phone:{
                    required: "Заполните поле",
                    regex: "Заполните корректно номер",
                },
                city: {
                    required: "Заполните поле",
                },
                adress: {
                    required: "Заполните поле",
                },
                index: {
                    required: "Заполните поле",
                    regex: "Введите 5 чисел",
                },
                index_for: {
                    required: "Заполните поле",
                },
                city_for: {
                    required: "Заполните поле",
                },
                adress_for: {
                    required: "Заполните поле",
                },
                order: {
                    required: "Заполните поле",
                },
                date_from: {
                    required: "Заполните поле",
                },
                date_range: {
                    required: "Заполните поле",
                },
                area: {
                    required: "Заполните поле",
                },
                pay_order: {
                    required: "Заполните поле",
                },
                room_type: {
                    required: "Заполните поле",
                },
                distance: {
                    required: "Заполните поле",
                },
                elevator: {
                    required: "Заполните поле",
                },
                number_floors: {
                    required: "Заполните поле",
                },
                number_room: {
                    required: "Заполните поле",
                },
                number_people: {
                    required: "Заполните поле",
                },
                transport: {
                    required: "Заполните поле",
                },
                r_type: {
                    required: "Заполните поле",
                },
                distance_parking: {
                    required: "Заполните поле",
                },
                presence_elevator: {
                    required: "Заполните поле",
                },
                n_floor: {
                    required: "Заполните поле",
                },
                means_transport: {
                    required: "Заполните поле",
                },
                password_new:{
                    required: "Введите пароль",
                    regex: "Минимум 8 символов, буквы разного региста, цифры",
                },
                password_repeat: {
                    required: "Повторите пароль"

                }

            }
        });
    }
    this.validate();
}


function Password() {
    this.password_matching = function () {
        $(".profile-but button").attr('disabled', true).removeClass('hover-el');
        $("#password2").keyup(validate);

        function validate() {
            var password1 = $("#password1").val();
            var password2 = $("#password2").val();

            if (password1 == password2) {
                $(".profile-but button").removeAttr('disabled').addClass('hover-el');
                $(".profile-but").addClass('active_but_lk');
                $("#validate-status").text("");
            }
            else {
                $("#validate-status").text("Пароли не совпадают");
            }
        }
    }

    this.password_matching();
}
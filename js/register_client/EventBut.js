function EventOneBut() {
    this.eventBut = function () {
        $('.btnSubmit').on('click', function () {
            // Validate();
            if(!$(".register  :valid").valid()) return false;
            $('html, body').animate({scrollTop: 0}, 500);
            $('.btnSubmit').submit();
        })
    }
    this.popupUse = function(){
        $("a[href=#js-terms-use]").fancybox();
    }
    this.eventBut();
    this.popupUse();
}
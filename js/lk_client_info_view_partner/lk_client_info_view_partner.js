$(function () {
    slider_company();
    map_company();
    scroll_rev();
    popup_slider_foto();
    open_popup();
});

function open_popup() {
    $("a[href=#js_popup_slider_foto]").fancybox();
}
function popup_slider_foto() {
    $.fancybox.defaults.afterShow = function(){
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            slideshow: false,
            itemWidth: 140,
            itemMargin: 6,
            asNavFor: '#slider'
        });
        $('#slider').flexslider({
            animation: "slide",
            controlNav: true,
            navigation:false,
            slideshow: false,
            sync: "#carousel"
        });
    };
}

function scroll_rev() {
    $('.right-col .reviews a[href*=#]').on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top  - 70
        }, {
            duration: 500,
            easing: "swing"
        });
        e.preventDefault();
        return false;
    });

};

function map_company() {
    var center = [53.036176, 8.640440];
    var markerImage = new google.maps.MarkerImage(
        "img/aboutUs/block4/map-placeholder.svg",
        null, /* size is determined at runtime */
        null, /* origin is 0,0 */
        null, /* anchor is bottom center of the scaled image */
        new google.maps.Size(42, 68)
    );

    $('#map_adress_partner_company')
        .gmap3({
            center: center,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })
        .marker(
            {
                position: [53.036176, 8.640440], icon: markerImage
            })

}

function slider_company() {
    $(".slider-partner-company ").owlCarousel({
        slidesToShow: 1,
        slidesToScroll: 1,
        loop: true,
        marginleft: 10,
        marginright: 10,
        autoHeight: true,
        nav: true,
        item: 1,
        dots: true,
        navText: ["", ""],
        responsive: {
            0: {
                items: 1
            },
            993: {
                items: 2
            },
            1200: {
                items: 3
            },
        }
    });
}


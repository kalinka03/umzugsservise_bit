function NexTab() {
    this.next = function () {
        $('.btnNext_sklad').on('click', function (e) {
            e.preventDefault();
            if(!$(".geography-block  :valid").valid()) return false;
            if(ConfirmDimensionsGlobal){
                if ($(".addThings").is(":visible")) {
                    if (!$(".addThings :valid").valid()) return false;
                }
                $(this).parents('.content-choice').removeClass('active').next().addClass('active');
                $('html, body').animate({scrollTop: 0}, 500);
                $(this).parents('.section-choice').find('.nav-tabs button.active').removeClass('active').next().addClass('active');
            }else{
                ConfirmDimensionsGlobal = true;

            }
        })
    };
    this.next();
}
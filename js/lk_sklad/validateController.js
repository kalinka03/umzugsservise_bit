function ValidateForm() {
    this.validate = function () {
        $("form").validate({
            rules: {
                name: {
                    required: true
                },
                surname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    regex: /([\+\-\(\)0-9]{11,16})\_{0,4}$/g
                },
                city: {
                    required: true,
                },
                index: {
                    required: true,
                    regex: /^([\(\)\+\- ]{0,2}[\d]){5,50}$/g
                },
                adress: {
                    required: true
                },
                city_for: {
                    required: true
                },
                index_for: {
                    required: true

                },
                adress_for: {
                    required: true
                },
                order: {
                    required: true
                },
                date_from: {
                    required: true
                },
                date_for: {
                    required: true
                },
                date_range: {
                    required: true
                },
                area: {
                    required: true
                },
                pay_order: {
                    required: true
                },
                room_type: {
                    required: true
                },
                distance: {
                    required: true
                },
                elevator: {
                    required: true
                },
                number_floors: {
                    required: true
                },
                number_room: {
                    required: true
                },
                number_people: {
                    required: true
                },
                transport: {
                    required: true
                },
                r_type: {
                    required: true
                },
                distance_parking: {
                    required: true
                },
                presence_elevator: {
                    required: true
                },
                n_floor: {
                    required: true
                },
                means_transport: {
                    required: true
                },
                name_company: {
                    required: true
                },
                registration_form: {
                    required: true
                },
                company_employees: {
                    required: true
                },
                file: {
                    required: true
                },
                "qty[]": {
                    required: true
                },
                "l_other[]":{
                    required: true,
                    number: true,
                    step: false
                },
                "h_other[]":{
                    required: true,
                    number: true,
                    step: false
                },
                "v_other[]":{
                    required: true,
                    number: true,
                    step: false
                }
            },
            messages: {
                name: {
                    required: "Заполните поле"
                },
                "qty[]": {
                    required: "Заполните поле"
                },
                surname: {
                    required: "Заполните поле"
                },
                email: {
                    required: "Заполните поле",
                    email: "Введите  корректный адрес"
                },
                phone: {
                    required: "Заполните поле",
                    regex: "Заполните корректно номер"
                },
                city: {
                    required: "Заполните поле"
                },
                adress: {
                    required: "Заполните поле"
                },
                index: {
                    required: "Заполните поле",
                    regex: 'Введите 5 цифр'
                },
                index_for: {
                    required: "Заполните поле"
                },
                city_for: {
                    required: "Заполните поле"
                },
                adress_for: {
                    required: "Заполните поле"
                },
                order: {
                    required: "Заполните поле"
                },
                date_from: {
                    required: "Заполните поле"
                },
                date_for: {
                    required: "Заполните поле"
                },
                date_range: {
                    required: "Заполните поле"
                },
                area: {
                    required: "Заполните поле"
                },
                pay_order: {
                    required: "Заполните поле"
                },
                room_type: {
                    required: "Заполните поле"
                },
                distance: {
                    required: "Заполните поле"
                },
                elevator: {
                    required: "Заполните поле"
                },
                number_floors: {
                    required: "Заполните поле"
                },
                number_room: {
                    required: "Заполните поле"
                },
                number_people: {
                    required: "Заполните поле"
                },
                transport: {
                    required: "Заполните поле"
                },
                r_type: {
                    required: "Заполните поле"
                },
                distance_parking: {
                    required: "Заполните поле"
                },
                presence_elevator: {
                    required: "Заполните поле"
                },
                n_floor: {
                    required: "Заполните поле"
                },
                means_transport: {
                    required: "Заполните поле"
                },
                name_company: {
                    required: "Заполните поле"
                },
                registration_form: {
                    required: "Заполните поле"
                },
                company_employees: {
                    required: "Заполните поле"
                },
                file: {
                    required: "Заполните поле"
                },
                "l_other[]":{
                    required: "Заполните",
                    number: "",
                    step: ""
                },
                "h_other[]":{
                    required: "Заполните",
                    number: "",
                    step: ""
                },
                "v_other[]":{
                    required: "Заполните",
                    number: "",
                    step: ""
                }
            }
        });
    }
    this.validate();
}
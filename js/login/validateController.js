
$(function () {
    ValidateForm();
});

function ValidateForm() {
        $("form").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password_new: {
                    required: true,
                    // regex: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g,
                },
            },
            messages: {
                email: {
                    required: "Заполните поле",
                    email: "Введите корректный email"
                },
                password_new:{
                    required: "Введите пароль",
                    // regex: "Минимум 8 символов, буквы разного региста, цифры",
                },
            }
        });

    // this.submit = function () {
    //     $('.login-btn').on('click', function () {
    //         // if(!$(".entrance-login :valid").valid()) return false;
    //         $('.btnSubmit').submit();
    //     })
    // }
    // this.submit();
}
$(function () {
    star_price();
    quality_order();
    quality_work_employees();
    quality_service();
    validation();
    $.validator.addMethod("validcb1", function(value){
        if ($(".recommend-company input[tupe=radio]:checked").length > 0) return true
        else return false;
    },"");
})
    function validation() {
        $(".give_feedback").validate({
            rules: {
                radio_test: {
                    required: true,
                },
                stars_check:{
                    validcb1: true,
                }
                // recomed: {
                //     required: function () {
                //         $('.recommend-company input[tupe=radio]').each(function(){
                //
                //
                //
                //             if($(this).is(':checked')) {
                //                 return true;
                //             }
                //             return false;
                //         })
                //         },
                //
                //     minlength: 1,
                //
                // }
            },
            messages: {
                radio_test: {
                    required: "Заполните поле",
                },
                // recomed: {
                //     required: "Выберите один из елементов",
                // },
                stars_check: {
                    validcb1 : "отметьте один из флажков"
                }
            }
        })
    }


function quality_service() {
    var $rezult = $('.quality_service input[name=stars]').val() - 1;
    $('.quality_service .checkmark-star').each(function (a) {
        if (a <= $rezult) {
            $(this).addClass('active_star');
        }
    });
    $('.quality_service .checkmark-star').hover(  function () {
        var $val_star = $(this).index();
        $('.quality_service input[name=stars]').val($val_star + 1);
        if ($val_star == 4) {
            $(this).parents('.quality_service').find('.hint_star').text('Превосходно');
        }
        if ($val_star == 3) {
            $(this).parents('.quality_service').find('.hint_star').text('Хорошо');
        }
        if ($val_star == 2) {
            $(this).parents('.quality_service').find('.hint_star').text('Нормально');
        }
        if ($val_star == 1) {
            $(this).parents('.quality_service').find('.hint_star').text('Удовлетворительно');
        }
        if ($val_star == 0) {
            $(this).parents('.quality_service').find('.hint_star').text('Неудовлетворительно');
        }
        $(this).parents('.quality_service').find('.characteristic').show();
    },         function() {
        $(this).parents('.quality_service').find('.characteristic').hide();
    })
    $('.quality_service .checkmark-star ').hover( function () {
        $('.quality_service .checkmark-star ').removeClass('active_star');
        var $rezult = $(this).index();

        $('.quality_service .checkmark-star ').each(function (a) {
            if (a <= $rezult) {
                $(this).addClass('active_star');
            }

        })
    })

};
function quality_work_employees() {
    var $rezult = $('.quality_work_employees input[name=stars]').val() - 1;
    $('.quality_work_employees .checkmark-star').each(function (a) {
        if (a <= $rezult) {
            $(this).addClass('active_star');
        }
    });
    $('.quality_work_employees .checkmark-star').hover(function () {
        var $val_star = $(this).index();
        $('.quality_work_employees input[name=stars]').val($val_star + 1);
        if ($val_star == 4) {
            $(this).parents('.quality_work_employees').find('.hint_star').text('Превосходно');
        }
        if ($val_star == 3) {
            $(this).parents('.quality_work_employees').find('.hint_star').text('Хорошо');
        }
        if ($val_star == 2) {
            $(this).parents('.quality_work_employees').find('.hint_star').text('Нормально');
        }
        if ($val_star == 1) {
            $(this).parents('.quality_work_employees').find('.hint_star').text('Удовлетворительно');
        }
        if ($val_star == 0) {
            $(this).parents('.quality_work_employees').find('.hint_star').text('Неудовлетворительно');
        }
        $(this).parents('.quality_work_employees ').find('.characteristic').show();
    },
        function() {
            $(this).parents('.quality_work_employees ').find('.characteristic').hide();
        });
    $('.quality_work_employees .checkmark-star ').hover(function () {
        $('.quality_work_employees .checkmark-star ').removeClass('active_star');
        var $rezult = $(this).index();

        $('.quality_work_employees .checkmark-star ').each(function (a) {
            if (a <= $rezult) {
                $(this).addClass('active_star');
            }

        })
    })

};
function quality_order() {
    var $rezult = $('.quality_order input[name=stars]').val() - 1;
    $('.quality_order .checkmark-star').each(function (a) {
        if (a <= $rezult) {
            $(this).addClass('active_star');
        }
    });
    $('.quality_order .checkmark-star').hover(function () {
        var $val_star = $(this).index();
        $('.quality_order input[name=stars]').val($val_star + 1);
        if ($val_star == 4) {
            $(this).parents('.quality_order').find('.hint_star').text('Превосходно');
        }
        if ($val_star == 3) {
            $(this).parents('.quality_order').find('.hint_star').text('Хорошо');
        }
        if ($val_star == 2) {
            $(this).parents('.quality_order').find('.hint_star').text('Нормально');
        }
        if ($val_star == 1) {
            $(this).parents('.quality_order').find('.hint_star').text('Удовлетворительно');
        }
        if ($val_star == 0) {
            $(this).parents('.quality_order').find('.hint_star').text('Неудовлетворительно');
        }
        $(this).parents('.quality_order').find('.characteristic').show();
    },        function() {
        $(this).parents('.quality_order').find('.characteristic').hide();
    });
    $('.quality_order .checkmark-star ').hover(function () {
        $('.quality_order .checkmark-star ').removeClass('active_star');
        var $rezult = $(this).index();

        $('.quality_order .checkmark-star ').each(function (a) {
            if (a <= $rezult) {
                $(this).addClass('active_star');
            }

        })
    })

};

function star_price() {
    var $rezult = $('.overall-price input[name=stars]').val() - 1;
    $('.overall-price .checkmark-star').each(function (a) {
        if (a <= $rezult) {
            $(this).addClass('active_star');
        }

    });
    $('.overall-price .checkmark-star').unbind('hover');
    $('.overall-price .checkmark-star').hover(function () {
        var $val_star = $(this).index();
        $('.overall-price input[name=stars]').val($val_star + 1);
        if ($val_star == 4) {
            $(this).parents('.overall-price').find('.hint_star').text('Превосходно');
        }
        if ($val_star == 3) {
            $(this).parents('.overall-price').find('.hint_star').text('Хорошо');
        }
        if ($val_star == 2) {
            $(this).parents('.overall-price').find('.hint_star').text('Нормально');
        }
        if ($val_star == 1) {
            $(this).parents('.overall-price').find('.hint_star').text('Удовлетворительно');
        }
        if ($val_star == 0) {
            $(this).parents('.overall-price').find('.hint_star').text('Неудовлетворительно');
        }
        $(this).parents('.overall-price').find('.characteristic').show();
    },
        function() {
            $(this).parents('.overall-price').find('.characteristic').hide();
        })

    $('.overall-price .checkmark-star ').hover( function () {
        $('.overall-price .checkmark-star ').removeClass('active_star');
        var $rezult = $(this).index();

        $('.overall-price .checkmark-star ').each(function (a) {
            if (a <= $rezult) {
                $(this).addClass('active_star');
            }
        })

    })
};
function DownloadPhoto() {
    this.addPhoto = function () {
        ConfirmDimensionsGlobal = true;
        $('.foto-block .add-plus').unbind('click');
        $('.foto-block .add-plus').click(function () {
            $('.foto-block').append('<div class="img-grup img-wrapp__none_img">\n' +
                '                               <div class="img-wrapp">\n' +
                '                                   <input type="file" style="display: none;" name="gallery[]" accept="image/jpeg,image/png,image/jpg">' +
                '                                   <span><span class="clouse-bg"></span></span>\n' +
                '                                   <img src="">\n' +
                '                               <div class="del-img">\n' +
                '                               </div>\n' +
                '                               </div>\n' +
                '                           </div>');
            $(this).parent('.foto-block').find('.img-grup:last-child input').click();
            binds();
            return false;
        });

        $('form').submit(function () {
            var $inps = $('.img-grup input');
            $inps.each(function () {
                var $this = $(this);
                if ($this[0].files.length == 0) {
                    $this.parents('.img-grup').remove();
                }
            });
        });

        var binds = function () {
            var $inps = $('.foto-block .img-grup input');
            var $close = $('.del-img');
            $close.unbind('click');
            $inps.unbind('change');
            $inps.change(function () {
                var $input = $(this);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $input.parent().find('img').attr('src', e.target.result);

                    if ($input[0].files.length !== 0)
                        $input.parents('.img-wrapp__none_img').removeClass('img-wrapp__none_img');
                };
                reader.readAsDataURL($input[0].files[0]);
            });
            $close.click(function () {
                $(this).parents('.img-grup').remove();
            })
        }
    }

    this.addPhoto();
}
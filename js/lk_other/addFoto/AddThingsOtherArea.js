function EventAreaThingOther() {
    this.areaThingsOther = function () {
        if ($('.other_spec_dim').is(":visible")) {
            $(".things_other input[id=v_other]").blur(function () {
                var l = $(this).parents('.r-col').find('.col-dim input[id=l_other]').val();
                var h = $(this).parents('.r-col').find('.col-dim input[id=h_other]').val();
                var v = $(this).parents('.r-col').find('.col-dim input[id=v_other]').val();
                console.log(l);
                var count = $(this).parents('.r-col').find('#count_parametr').val();
                var rez = parseFloat(l) * parseFloat(h) * parseFloat(v);
                var multiplication = parseFloat(l) + 'x' + parseFloat(h) + 'x' + parseFloat(v);
                var all_param = count * rez;
                $(this).parents('.r-col').find('input[name=rezult_other]').val(rez);
                var rezult = $(this).parents('.r-col').find('input[name=rezult_other]').val();
                $(this).parents('.r-col').find('input[name=rezult_all]').val(all_param);
                var rounding = (parseInt(rezult * 100)) / 100;
                $(this).parents('.r-col').find('#rez_delimiter_other').val(parseFloat(rounding).toFixed(2));
                var rez_del = $(this).parents('.r-col').find('#rez_delimiter_other').val();
                // var rez_str = rez_del.replace(/-/g, ',');
                $(this).parents('.r-col').find('#rez_ar_other').val(rez_del);
                var rez_str = rez_del.replace('.', ',');
                $(this).parents('.r-col').find('#rez_ar_other').val(rez_str);
                $(this).parents('.r-col').find('#parametr_specify_other').val(rez_str);
                $(this).parents('.r-col').find('#rezult_areas').val(rounding);
                $(this).parents('.r-col').find('input[name=rezult_hid]').val(rounding);
                $(this).parents('.r-col').find('#parametr_specify_other').css('display', 'inline-block');
                $(this).parents('.r-col').find('.measurements').css('display', 'inline-block');
                $(this).parents('.r-col').find('input[name="multiplication"]').val(multiplication);
                var mult = $(this).parents('.r-col').find('input[name="multiplication"]').val();
                $(this).parents('.r-col').find('input[name="multiplication_rez"]').val(mult);
                $(this).parents('.r-col').find('.specify-dimensions').hide();
                $(this).parents('.r-col').find('.parametr').show();
                if ($('.all-quantity-things').is(':visible')) {
                    if (($(".hover_elements").length>0)) {
                        AreaSk = new showAreaSkThings();
                    }
                }
            })
            if (($(".hover_elements").length>0)) {
                AreaSk = new showAreaSkThings();
            }
        }
    }
    this.areaThingsOther();
}
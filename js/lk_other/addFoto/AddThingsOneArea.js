function EventAreaThingOne() {
    this.areaThingsOne = function () {
            $(".add-things").on('click', function (e) {
                e.preventDefault();



                if($('.one_thing_parametr').is(":visible")) {
                    s = '';

                    var l = $(this).parents('.one_thing_parametr').find('.col-dim input[id=l]').val();
                    var h = $(this).parents('.one_thing_parametr').find('.col-dim input[id=h]').val();
                    var v = $(this).parents('.one_thing_parametr').find('.col-dim input[id=v]').val();
                    var count = $(this).parents('.r-col').find('#count_parametr').val();
                    if (l !== s && h !== s && v !== s) {
                        var rez = parseFloat(l) * parseFloat(h) * parseFloat(v);
                        var all_param = count * rez;
                        var multiplication = parseFloat(l) + 'x' + parseFloat(h) + 'x' + parseFloat(v);
                        $(this).parents('.r-col').find('input[name=rezult_hid]').val(rez);
                        $(this).parents('.r-col').find('input[name=rezult_all]').val(all_param);
                        var rezult = $(this).parents('.r-col').find('input[name=rezult_hid]').val();
                        var rounding = (parseInt(rezult * 100)) / 100;
                        $(this).parents('.r-col').find('#rez_delimiter').val(parseFloat(rounding ).toFixed(2));
                        var rez_del = $(this).parents('.r-col').find('#rez_delimiter').val();
                        var rez_str = rez_del.replace('.', ',');
                       $(this).parents('.r-col').find('#rez_ar').val(rez_str);
                        var rezult_delim = $(this).parents('.r-col').find('#rez_ar').val();
                        $(this).parents('.r-col').find('#parametr_specify').val(rezult_delim);
                        $(this).parents('.r-col').find('#things_area_other').val(rez_del);
                        $(this).parents('.r-col').find('input[name="multiplication"]').val(multiplication);
                        var mult = $(this).parents('.r-col').find('input[name="multiplication"]').val();
                        var rez_mult = mult.replace('.', ',');
                        $(this).parents('.r-col').find('input[name="multiplication_all"]').val(rez_mult);
                        $(this).parents('.r-col').find('input[name="multiplication_rez"]').val(mult);
                        $(this).parents('.r-col').find('.specify-dimensions').hide();
                        $(this).parents('.r-col').find('.parametr').show();
                        if ($('.all-quantity-things').is(':visible')) {
                            if (window.location.pathname == '/order_sklad.php') {
                                AreaSk = new showAreaSkThings();
                            }
                        }
                    }
                }
                if (window.location.pathname == '/order_sklad.php') {
                    AreaSk = new showAreaSkThings();
                }
                $(this).parents(".r-col").find(".confirm_meanings").hide();
            });
    };
    this.areaThingsOne();
}
$(function () {
    changeIndexForStorage();
});

function changeIndexForStorage() {
    var projects = [
        {
            label: "01067",
            desc: "Dresden",
        },
        {
            label: "01468",
            desc: "Moritzburgttttttttttttttttttttttttttt",
        },
        {
            label: "01589",
            desc: "Riesa",
        }
    ];

    $( ".change_index_sk" ).autocomplete({
        minLength: 0,
        source: projects,
        focus: function( event, ui ) {
            $( ".change_index_sk" ).val( ui.item.label );
            return false;
        },
        select: function( event, ui ) {
            $('.change_index_sk').val( ui.item.label );
            $('.v-d-city .name_city_for_index').val( ui.item.desc);
            $('.change_index_sk').keyup();
            $('.name_city_for_index').keyup();
            return false;
        }
    })
        .autocomplete( "instance" )._renderItem = function( div, item ) {
        return $( "<div class='list_things_item'>")
            .append( "<li class='change_city_li'>" + item.label + " , " + item.desc + "</li>" )
            .appendTo( div);
    };

}
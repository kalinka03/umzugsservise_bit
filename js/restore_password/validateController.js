
$(function () {
    Validate = new ValidateForm();
});


function ValidateForm() {
    this.validate = function () {
        $("form").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                }
            },
            messages: {
                email: {
                    required: "Заполните поле",
                    email: "Введите коректный email"
                }
            }
        });
    }
    this.submit = function () {
        $('.login-btn').on('click', function () {
            if(!$(".center-info  :valid").valid()) return false;
            $('.btnSubmit').submit();
        })
    }
    this.submit();
    this.validate();
}
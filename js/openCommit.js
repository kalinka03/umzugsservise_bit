$(function () {
    open_reviews_company();
    open();
    close();
});
function open() {
    $(".more_reviews_company_partner").click(function (e) {
        e.preventDefault();
        $(this).css('display','none');
        $(this).parents('.r-col').find('.text-review-company').addClass('height_comment');
        $(this).parents('.r-col').find('.close_reviews_company_partner').css('display','inline');
    });
}
function close() {
    $(".some-review-company .close_reviews_company_partner").click(function (e) {
        e.preventDefault();
        $(this).css('display','none');
        $(this).parents('.r-col').find('.more_reviews_company_partner').css('display','inline');
        $('.text-review-company').removeClass('height_comment');
    });
}
function open_reviews_company() {
    $(".text-review-company").each(function () {
        var review_full = $(this).html();
        var review = review_full;

        if (review.length > 800) {
            $(this).parents('.some-review-company').find('.more_reviews_company_partner').css('display','inline');
        }

    })
}
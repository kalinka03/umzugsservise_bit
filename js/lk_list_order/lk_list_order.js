$(function () {
    tab();
    open_parametr();
    hide_list();
    popup();
    accepted();
    reject();
    open_how_work();
    inquiry();
    lenght_city();
    change_application();
    send_save_change();
    close_popup();
    send_form_contact();
    confirm_offer();
    сancel_application();
    // refuse_offer();
    open_popup_foto_gallery();
    open_popup_foto();
    // open_popup_foto_downloaded();
});
function open_popup_foto() {
    $("a[href=#js_popup_foto_things_application]").fancybox();
    $("a[href=#js_popup_foto_things_downloaded]").fancybox();
}
function  open_popup_foto_gallery() {
    $.fancybox.defaults.afterShow = function(){
        $('#carousel_things').flexslider({
            animation: "slide",
            controlNav: false,
            slideshow: false,
            itemWidth: 140,
            itemMargin: 6,
            asNavFor: '#slider_things'
        });
        $('#slider_things').flexslider({
            animation: "slide",
            controlNav: true,
            navigation:false,
            slideshow: false,
            sync: "#carousel_things"
        });
        $('#carousel_things_downloaded').flexslider({
            animation: "slide",
            controlNav: false,
            slideshow: false,
            itemWidth: 140,
            itemMargin: 6,
            asNavFor: '#slider_things_downloaded'
        });
        $('#slider_things_downloaded').flexslider({
            animation: "slide",
            controlNav: true,
            navigation:false,
            slideshow: false,
            sync: "#carousel_things_downloaded"
        });
    };
}
function  open_popup_foto_downloaded() {
    $.fancybox.defaults.afterShow = function(){
        $('#carousel_things_downloaded').flexslider({
            animation: "slide",
            controlNav: false,
            slideshow: false,
            itemWidth: 140,
            itemMargin: 6,
            asNavFor: '#slider_things_downloaded'
        });
        $('#slider_things_downloaded').flexslider({
            animation: "slide",
            controlNav: true,
            navigation:false,
            slideshow: false,
            sync: "#carousel_things_downloaded"
        });
    };
}
// function  refuse_offer() {
//     $('.choice-offer .refuse_offer').click(function(){
//         //После отправки формы из попапа
//         $(this).parents('.company_offer').remove();
//     })
// }
function сancel_application() {
    $('.сancel_application').click(function(){
        //После отправки формы из попапа
        $('.application_canceled').show();
        $('.application_top .right-col .confirmed-proposal ').remove();
    })
}



function confirm_offer() {
    $('.yes_confirm_offer').click(function(){
        // $("form").submit();
        //После отправки формы открывается fansybox
    $(this).parents('confirm_offer').find('.hide_confirmed-proposal').show();
     $(this).parents('confirm_offer').find('.but ').remove();
    })
}



function send_form_contact() {
    $('.down_company_partner_all .choice-offer .to_come_contact').click(function(){
        //После отправки формы открывается fansybox
        $("a[href=#js_come_contact]").fancybox();
        $(this).parents('.down_company_partner_all').find('.after_send_form_confirmed').show();
        $(this).parents('.down_company_partner_all').find('.choice-offer').remove();
    })
}



function close_popup() {
    $('.refuse_offer_close button').click(function(){
        // $("#js_refuse_offer").close();
        $.fancybox.close();
    })
}


function change_application() {
$('.change_application_my').click(function(e){
    e.preventDefault();
    $(this).hide();
    $('.save_change_application').show();
    $('.list .text .date_application_form').hide();
    $('.list .text .sk_calendar').show();
    $('.commit_send_application').hide();
    $('.commit_send_company_application').show();
})

}
function send_save_change() {
    $(".save_change_application button").click(function(){
            $("form").submit();
    })
}






function lenght_city() {
    // var center = [53.036176, 8.640440];
    // var markerImage =  new google.maps.MarkerImage(
    //     "img/aboutUs/block4/map-placeholder.svg",
    //     null, /* size is determined at runtime */
    //     null, /* origin is 0,0 */
    //     null, /* anchor is bottom center of the scaled image */
    //     new google.maps.Size(42, 68)
    // );
    //
    // $('#google_map')
    //     .gmap3({
    //         center: center,
    //         zoom: 15,
    //         mapTypeId: google.maps.MapTypeId.ROADMAP
    //     })
    //     .marker(
    //         {
    //             position: [53.036176, 8.640440], icon: markerImage
    //         })

    var geocoder = new google.maps.Geocoder();
    var city1_name = $('.found_lg_city_from').text();

    geocoder.geocode({'address': city1_name}, function (results, status) {
        // if (status == google.maps.GeocoderStatus.OK) {
        var city1_lat = results[0].geometry.location.lat();
        var city1_lng = results[0].geometry.location.lng();
        // } else {
        //     alert("Something got wrong " + status);
        // }
        var city2_name = $('.found_lg_city_for').text();
        geocoder.geocode({'address': city2_name}, function (results, status) {
            // if (status == google.maps.GeocoderStatus.OK) {
            var city2_lat = results[0].geometry.location.lat();
            var city2_lng = results[0].geometry.location.lng();
            // } else {
            //     alert("Something got wrong " + status);
            // }

            var map = new google.maps.Map(document.getElementById('google_map'), {
                zoom: 7,
                center: {lat: city1_lat, lng: city1_lng},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var flightPlanCoordinates = [
                {lat: city1_lat, lng: city1_lng},
                {lat: city2_lat, lng: city2_lng}
            ];
            var flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: '#ff8f3d',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });

            flightPath.setMap(map);

            var p1 = new google.maps.LatLng(city1_lat, city1_lng);
            var p2 = new google.maps.LatLng(city2_lat, city2_lng);

            $(window).load(function () {
                $('.content-news .application-descript-top .descript .list .text span.distance').text(calcDistance(p1, p2));
            });

            function calcDistance(p1, p2) {
                return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
            }
        });

    });



    // var markerImage = new google.maps.MarkerImage(
    //     "img/aboutUs/block4/map-placeholder.svg",
    //     null,
    //     null,
    //     null,
    //     new google.maps.Size(42, 68)
    // );


    // var markerImage = {
    //     attraction: new google.maps.MarkerImage(
    //         "img/aboutUs/block4/map-placeholder.svg",
    //         null, /* size is determined at runtime */
    //         null, /* origin is 0,0 */
    //         null, /* anchor is bottom center of the scaled image */
    //         new google.maps.Size(22, 32)
    //     ),
    //     beaches: new google.maps.MarkerImage(
    //         "img/aboutUs/block4/map-placeholder.svg",
    //         null, /* size is determined at runtime */
    //         null, /* origin is 0,0 */
    //         null, /* anchor is bottom center of the scaled image */
    //         new google.maps.Size(22, 32)
    //     ),
    // }
    // var markers = {};
    //
    // var marker = new google.maps.Marker({
    //     position: endPoint,
    //     map: map
    // });


}


function inquiry() {
    $('.request-price button').on('click', function () {
        //После отправки появляется текст //
        $(this).hide();
        $(this).parents('.request-price').find('.request-sent').show();
    })
}


function open_how_work() {
    $('.block_how_it_works').on('click', function (e) {
        e.preventDefault();
        $('.hover_how_works').show();
        $('.block_how_it_works').hide();
    })
    $('.hover_how_works .close-block-work').on('click', function (e) {
        e.preventDefault();
        $('.hover_how_works').hide();
        $('.block_how_it_works').show();
    })
}

function accepted() {
    $('.action-btn button.accepted').on('click', function () {
        $('.proposal-accepted').show();
        $('.offer-action').hide();
    })

}

function reject() {
    $('.action-btn button.reject').on('click', function () {
        $('.offer-rejected').show();
        $('.offer-action').hide();
    })

}

function popup() {
    $("a[href=#js_map]").fancybox();
    $("a[href=#js_сancel_application]").fancybox();
    $("a[href=#js_reason_date]").fancybox();
    $("a[href=#js_confirm_offer]").fancybox();
    $("a[href=#js_refuse_offer]").fancybox();
    $("a[href=#js_come_contact]").fancybox();
}

function tab() {
    $('.tab-content>div:not(":first-of-type")').hide();
    $('.tab-menu button').each(function (i) {
        $(this).attr('data-tab', 'tab' + i);
    });
    $('.tab-content .content-choice').each(function (i) {
        $(this).attr('data-tab', 'tab' + i);
    });
    $('.tab-menu button').on('click', function (e) {
        e.preventDefault();
        var dataTab = $(this).data('tab');
        var getWrapper = $(this).closest('.choice-tab-list-order');
        getWrapper.find('.tab-menu button').removeClass('active');
        $(this).addClass('active');
        getWrapper.find('.tab-content>.content-choice').hide();
        getWrapper.find('.tab-content>.content-choice[data-tab=' + dataTab + ']').show();
    });
}

function open_parametr() {
    $('.open_param').on('click', function (e) {
        e.preventDefault();
        $('.conditions_moving').show();
        $('.open_param').hide();

    })

}

function hide_list() {
    $('.hide_list').on('click', function (e) {
        e.preventDefault();
        $('.open_param').show();
        $('.conditions_moving').hide();
    })
}